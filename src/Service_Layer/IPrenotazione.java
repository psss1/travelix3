package Service_Layer;

public interface IPrenotazione {

	boolean creaPrenotazione(String infoPrenotazione, String username);
	
	String visualizzaPrenotazione(int idPrenotazione);

	String visualizzaListaPrenotazioni(String username);
	
	boolean pagaPrenotazione(int idPrenotazione);
	
	boolean eliminaPrenotazione(int idPrenotazione);

}