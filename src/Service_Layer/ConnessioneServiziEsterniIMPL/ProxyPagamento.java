package Service_Layer.ConnessioneServiziEsterniIMPL;

import Service_Layer.IPagamento;

public class ProxyPagamento implements IPagamento{

	public boolean effettuaPagamento() {
		return true;
	}

}