package Service_Layer.ConnessioneServiziEsterniIMPL;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;

import Service_Layer.IHotel;

public class ProxyHotel implements IHotel{

	public String url_dreamFactory = "https://travelix3.apps.dreamfactory.com/api/v2/travelix/_table/hotel";
	public String api_key = "8f0971afbe39862871e9fd2958f0b475ada110b9e5db0ac9eeef76387308d927";

	public void cambiaDisponibilitaHotel(int idHotel, String stato) {

		/*Cambia la disponibilitÓ dell'hotel in "si" o "no", a seconda del contenuto di "stato".
		 * Da usare quando un hotel viene prenotato, o quando una prenotazione viene cancellata.
		 */
		
		try {

			URL obj_put = new URL(url_dreamFactory);

			HttpURLConnection conn_put = (HttpURLConnection) obj_put.openConnection();

			conn_put.setRequestProperty("X-DreamFactory-API-Key", api_key);
			conn_put.setDoOutput(true);

			conn_put.setRequestMethod("PUT");

			String data = "{\"resource\":[{\"ID\":"+idHotel+",\"disponibilita\":\""+stato+"\"}]}";

			OutputStreamWriter out = new OutputStreamWriter(conn_put.getOutputStream());
			out.write(data); 
			out.close();

			new InputStreamReader(conn_put.getInputStream());

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();

		}

	}

	public String getRiepilogoHotel(int idHotel) {

		String risposta = null;

		try {

			String url_get =url_dreamFactory+"?filter=(ID="+idHotel+")";  

			URL obj_get = new URL(url_get);

			HttpURLConnection conn_get = (HttpURLConnection) obj_get.openConnection();

			conn_get.setRequestProperty("X-DreamFactory-API-Key",api_key);
			conn_get.setDoOutput(true);

			conn_get.setRequestMethod("GET");

			BufferedReader br = new BufferedReader(new InputStreamReader(conn_get.getInputStream(), "utf-8"));
			StringBuilder response_get = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response_get.append(responseLine.trim());
			}

			risposta = response_get.toString();

			if(risposta == null) {
				return null;
			}

			new InputStreamReader(conn_get.getInputStream());

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 

		return risposta;
	}
	

	public String getCatalogoHotel(String destination, String checkIn, String checkOut, int persone) {

		String risposta = null;

		try {

			String url_get =url_dreamFactory+"?filter=(luogo="+destination+")AND(check_in<="+checkIn+")AND(check_out>="+checkOut+")AND(persone="+persone+")AND(disponibilita=si)";  

			URL obj_get = new URL(url_get);

			HttpURLConnection conn_get = (HttpURLConnection)obj_get.openConnection();

			conn_get.setRequestProperty("X-DreamFactory-API-Key",api_key);
			conn_get.setDoOutput(true);

			conn_get.setRequestMethod("GET");

			BufferedReader br = new BufferedReader(new InputStreamReader(conn_get.getInputStream(), "utf-8"));
			StringBuilder response_get = new StringBuilder();
			String responseLine = null;
			while ((responseLine = br.readLine()) != null) {
				response_get.append(responseLine.trim());
			}

			risposta = response_get.toString();

			if(risposta == null) {
				return null;
			}

			new InputStreamReader(conn_get.getInputStream());

		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ProtocolException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (UnsupportedEncodingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		return risposta;
	}

}