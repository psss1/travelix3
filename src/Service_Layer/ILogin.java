package Service_Layer;

public interface ILogin {

	public String accedi(String username, String password);

	public boolean accediFacebook(String token);

}