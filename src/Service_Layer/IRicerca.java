package Service_Layer;

public interface IRicerca {

	String visualizzaCatalogoViaggi(String luogoOrigine, String luogoDestinazione,
			String checkIn, String checkOut, String numeroAdulti, String numeroBambini);

	String visualizzaRiepilogoViaggio(int idHotel, String idVolo);

}