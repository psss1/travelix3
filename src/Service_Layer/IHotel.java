package Service_Layer;

public interface IHotel {

	void cambiaDisponibilitaHotel(int idHotel, String stato);

	String getRiepilogoHotel(int idHotel);

	String getCatalogoHotel(String destination, String checkIn, String checkOut, int persone);

}