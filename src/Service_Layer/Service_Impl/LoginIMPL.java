package Service_Layer.Service_Impl;

import org.apache.commons.codec.digest.DigestUtils;
import org.json.JSONException;
import org.json.JSONObject;

import Data_Layer.DAOFactory;
import Data_Layer.Utenti.Utente;
import Data_Layer.Utenti.UtentiDAO;
import Service_Layer.IFacebook;
import Service_Layer.ConnessioneServiziEsterniIMPL.ProxyAccessoFacebook;

public class LoginIMPL implements Service_Layer.ILogin{

	public boolean accediFacebook(String token) {

		boolean is_valid = false;
		boolean esito=false;

		String application = " ";
		Integer expires = 0;

		long unixTime = System.currentTimeMillis() / 1000L;

		try {

			String rispostaFB = new String();

			IFacebook fb = new ProxyAccessoFacebook();

			rispostaFB = fb.autenticazioneFB(token);

			JSONObject obj = new JSONObject(rispostaFB);
			is_valid = obj.getJSONObject("data").getBoolean("is_valid");
			application = obj.getJSONObject("data").getString("application");
			expires = obj.getJSONObject("data").getInt("expires_at");
		} catch (JSONException e) {
			e.printStackTrace();
		}

		if (application.equals("travelix") && is_valid==true && expires>unixTime) {
			esito=true;
		}


		return esito;
	}

	public String accedi(String username, String password) {

		String JSONUtente_str = new String();

		try {

			String hashedpass = DigestUtils.sha1Hex(password);
			JSONObject JSONUtente = new JSONObject();
			boolean isAuthenticated=false;

			DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.UTENTI);
			UtentiDAO riverDAO = (UtentiDAO) mysqlFactory.getMYSQLDAO();

			Utente utente = riverDAO.getUtente(username);

			String passdb = utente.getPassword();

			//controllo se l'hash della password appena calcolato
			//� uguale a quello contenuto nel db

			if(passdb.equals(hashedpass)) {
				isAuthenticated = true;
			}

			else
				System.out.println("autenticazione fallita!");

			if(isAuthenticated) {
				JSONUtente.put("esito", "true");
				JSONUtente.put("nome",utente.getNome());
				JSONUtente.put("ruolo",utente.getRuolo());
			}

			else {
				JSONUtente.put("esito", "false");
			}

			JSONUtente_str=JSONUtente.toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return JSONUtente_str;
	}
}