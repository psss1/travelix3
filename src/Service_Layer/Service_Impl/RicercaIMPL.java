package Service_Layer.Service_Impl;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Service_Layer.IHotel;
import Service_Layer.IRicerca;
import Service_Layer.IVoli;
import Service_Layer.ConnessioneServiziEsterniIMPL.ProxyHotel;
import Service_Layer.ConnessioneServiziEsterniIMPL.ProxyVoli;

public class RicercaIMPL implements IRicerca{

	public String visualizzaCatalogoViaggi(String luogoOrigine, String luogoDestinazione, String checkIn, String checkOut, String numeroAdulti, String numeroBambini) {

		String array_str = new String();

		try {
			
			//Separo citt� e IATA Code presenti nel men� a tendina della ricerca
			//Es. Napoli-NAP
			//arr_departure[0] == Napoli
			//arr_departure[1] == NAP

			String arr_departure[] = luogoOrigine.split("-", 2); 
			String arr_destination[] = luogoDestinazione.split("-", 2);

			String nome_esteso_partenza = new String(arr_departure[0]);
			String nome_esteso_destinazione = new String(arr_destination[0]);

			String departure_IATA = new String(arr_departure[1]); //IATA Partenza
			String destination_IATA = new String(arr_destination[1]); //IATA Destinazione

			// -----------------------------INIZIALIZZAZIONE DEI PROXY E CONTROLLO RISULTATI VUOTI----------------------------------------------------------

			IVoli Pv = new ProxyVoli();

			String check_in = checkIn.replace("-", ""); // toglie il "-"
			String check_out = checkOut.replace("-", ""); // toglie il "-"

			String JSON_voli = Pv.getVoli(departure_IATA, destination_IATA, check_in, check_out, numeroAdulti,numeroBambini);

			IHotel Ph = new ProxyHotel();

			int numeroAdulti_int = Integer.parseInt(numeroAdulti);
			int numeroBambini_int = Integer.parseInt(numeroBambini);
			int persone = Integer.parseInt(numeroAdulti) + Integer.parseInt(numeroBambini); //Sommo adulti e bambini
			
			String JSON_hotel = Ph.getCatalogoHotel(nome_esteso_destinazione, checkIn, checkOut, persone);
			
			if (JSON_voli == null || JSON_hotel.compareTo("{\"resource\":[]}") == 0) { //nel caso il sistema non trovi voli o hotel
																					   //corrispondenti ai parametri di ricerca
																					   //dell'utente, verr� stampato un messaggio d'errore

				JSONArray errore_arr = new JSONArray();
				//errore.put("size", 0); //valore che sar� controllato dal client, funzione mostra_catalogo(), presente in scriptHome.js
				String errore_str = new String(errore_arr.toString()); 
				return errore_str; 
			}
			
			// ----------------------------------RECUPERO INFO VOLO----------------------------------------------------------------------------

			JSONObject obj_voli = new JSONObject(JSON_voli);
			JSONArray arrayFlight = obj_voli.getJSONArray("flightOffer");
			JSONObject flightOffer = arrayFlight.getJSONObject(0);
			JSONObject priceInfo = (JSONObject) flightOffer.get("pricingInfoSum");
			int prezzo_voli_tot = priceInfo.getInt("totalPriceAllPassengers"); //prezzo andata+ritorno

			String ID_volo = new String();
			ID_volo = String.join("-", departure_IATA, destination_IATA, check_in, check_out, numeroAdulti, numeroBambini,nome_esteso_partenza);
			// Esempio = NAP-AMS-20200610-20200617-02-00-Napoli

			//Calcolo differenza fra check-in e check-out per ottenere il numero di notti,
			//che verr� usato per mostrare il prezzo complessivo

			SimpleDateFormat myFormat = new SimpleDateFormat("yyyy-MM-dd");

			Date inizio = myFormat.parse(checkIn);
			Date fine = myFormat.parse(checkOut);

			int notti = (int)( (fine.getTime() - inizio.getTime()) / (1000 * 60 * 60 * 24) );

			// ----------------------------------RECUPERO INFO HOTEL----------------------------------------------------------------------------

			JSONObject obj_get_risposta = new JSONObject(JSON_hotel);
			JSONArray resource = obj_get_risposta.getJSONArray("resource");

			JSONObject jsonViaggi = new JSONObject();

			int size = resource.length(); //Numero di risultati trovati dalla query degli hotel
			
			JSONArray array = new JSONArray();

			int[] prezzi_hotel = new int[size];
			String[] nomi_hotel = new String[size];
			int[] prezzi_tot = new int[size];
			String[] immagini_hotel = new String[size];
			String[] rating_hotel = new String[size];
			int[] stelle_hotel = new int[size];
			int[] ID_hotel = new int[size];

			for (int i = 0; i < size; i++) {

				nomi_hotel[i] = resource.getJSONObject(i).getString("nome");
				prezzi_hotel[i] = resource.getJSONObject(i).getInt("prezzo");
				immagini_hotel[i] = resource.getJSONObject(i).getString("immagine");
				rating_hotel[i] = resource.getJSONObject(i).getString("rating");
				stelle_hotel[i] = resource.getJSONObject(i).getInt("stelle");
				prezzi_tot[i] = (prezzi_hotel[i] * notti) + prezzo_voli_tot;
				ID_hotel[i] = resource.getJSONObject(i).getInt("ID");

				JSONObject shortInfo = new JSONObject();
				JSONObject details = new JSONObject();
				JSONObject infoViaggio = new JSONObject();
				JSONObject box = new JSONObject();

				shortInfo.put("nome_hotel", nomi_hotel[i]);
				shortInfo.put("immagine_hotel", immagini_hotel[i]);
				shortInfo.put("rating_hotel", rating_hotel[i]);
				shortInfo.put("stelle_hotel", stelle_hotel[i]);
				shortInfo.put("prezzo_tot", prezzi_tot[i]);
				shortInfo.put("ID_hotel", ID_hotel[i]);
				shortInfo.put("ID_volo", ID_volo);
				shortInfo.put("numeroAdulti", numeroAdulti_int);
				shortInfo.put("numeroBambini", numeroBambini_int);
				
				box.put("shortInfo", shortInfo);
				box.put("details", details);
				infoViaggio.put("infoViaggio", box);
				
				array.put(infoViaggio);

			}

			array_str = array.toString();

		}
		catch(Exception e) {
			e.printStackTrace();
		}

		return array_str;

	}

	public String visualizzaRiepilogoViaggio(int id_hotel,String id_volo) {


		String jsonRiepilogo_str = new String();

		try {

			IHotel Ph = new ProxyHotel(); //Inizializzo i 2 proxy
			IVoli Pv = new ProxyVoli();

			// --------------------------------------RIEPILOGO HOTEL-----------------------------------------------------------------------

			String JSON_riepilogoHotel = Ph.getRiepilogoHotel(id_hotel); //Recupero i dati dell'hotel con una query sull'ID

			// --------------------------------------RIEPILOGO VOLO-----------------------------------------------------------------------

			/*Formato stringa = IATA_Code_Andata - IATA_Code_Ritorno - Data_Andata - DataRitorno - Adulti - Bambini - Citt� andata
			 * Esempio = NAP-AMS-20200610-20200617-02-00-Napoli
			 */

			String array_ID_volo[] = id_volo.split("-", 7);

			/* array_ID_volo[0] == NAP
			 * array_ID_volo[1] == AMS
			 * ...
			 */

			String departure = array_ID_volo[0];
			String destination = array_ID_volo[1];
			String check_in = array_ID_volo[2];
			String check_out = array_ID_volo[3];
			String adults = array_ID_volo[4];
			String children = array_ID_volo[5];
			String nome_esteso_partenza = array_ID_volo[6];

			//Rifaccio la query del volo
			String JSON_voli = Pv.getVoli(departure, destination, check_in, check_out, adults, children);

			JSONObject obj_hotel = new JSONObject(JSON_riepilogoHotel);
			JSONArray resource = obj_hotel.getJSONArray("resource");
			JSONObject hotel_obj = resource.getJSONObject(0);
			
			JSONObject box = new JSONObject();
			JSONObject infoViaggio = new JSONObject();
			JSONObject shortInfo = new JSONObject();
			JSONObject details = new JSONObject();
			
			JSONObject obj_volo = new JSONObject(JSON_voli);
			JSONArray arrayFlight = obj_volo.getJSONArray("flightOffer");
			JSONObject flightOffer = arrayFlight.getJSONObject(0);
			JSONObject priceInfo = (JSONObject) flightOffer.get("pricingInfoSum");
			int prezzoVolo = priceInfo.getInt("totalPriceAllPassengers"); //prezzo andata+ritorno
			
			DateFormat format = new SimpleDateFormat("yyyyMMdd");
			Date inizio = format.parse(check_in);
			Date fine = format.parse(check_out);
			int notti = (int) ( (fine.getTime() - inizio.getTime()) / (1000 * 60 * 60 * 24) );
			int prezzo_hotel = hotel_obj.getInt("prezzo");
			int prezzo_totale = (prezzo_hotel * notti) + prezzoVolo;
			
			shortInfo.put("nome_hotel", hotel_obj.getString("nome"));
			shortInfo.put("immagine_hotel", hotel_obj.getString("immagine"));
			shortInfo.put("rating_hotel", hotel_obj.getString("rating"));
			shortInfo.put("stelle_hotel", hotel_obj.getInt("stelle"));
			shortInfo.put("prezzo_tot", prezzo_totale);
			shortInfo.put("ID_hotel", id_hotel);
			shortInfo.put("ID_volo", id_volo);
			shortInfo.put("numeroAdulti", Integer.parseInt(adults));
			shortInfo.put("numeroBambini", Integer.parseInt(children));
			
			JSONObject outboundFlight_box = (JSONObject) flightOffer.get("outboundFlight");
			String departureDateTime_out = outboundFlight_box.getString("departureDateTime");
			String arrivalDateTime_out = outboundFlight_box.getString("arrivalDateTime");
			
			JSONObject inboundFlight_box = (JSONObject) flightOffer.get("inboundFlight");
			String departureDateTime_in = inboundFlight_box.getString("departureDateTime");
			String arrivalDateTime_in = inboundFlight_box.getString("arrivalDateTime");
			
			details.put("totalPriceAllPassengers", prezzoVolo);
			details.put("departureDateTime_out",departureDateTime_out);
			details.put("arrivalDateTime_out",arrivalDateTime_out);
			details.put("departureDateTime_in",departureDateTime_in);
			details.put("arrivalDateTime_in",arrivalDateTime_in);
			details.put("nome_esteso_partenza",nome_esteso_partenza);
			details.put("disponibilita",hotel_obj.getString("disponibilita"));
			details.put("numero_hotel",hotel_obj.getString("numero"));
			details.put("luogo_hotel",hotel_obj.getString("luogo"));
			details.put("check_in",hotel_obj.getString("check_in"));
			details.put("check_out",hotel_obj.getString("check_out"));
			details.put("indirizzo",hotel_obj.getString("indirizzo"));
			details.put("descrizione",hotel_obj.getString("descrizione"));
			details.put("prezzo_hotel",hotel_obj.getInt("prezzo"));
			
			box.put("shortInfo", shortInfo);
			box.put("details", details);
			
			infoViaggio.put("infoViaggio", box);

			jsonRiepilogo_str = infoViaggio.toString();

		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return jsonRiepilogo_str;
	}
	
	JSONArray inizializzaJSON_Catalogo(int size) throws JSONException {
		
		JSONArray arr = new JSONArray();
		JSONObject box = new JSONObject();
		JSONObject infoViaggio = new JSONObject();
		JSONObject shortInfo = new JSONObject();
		JSONObject details = new JSONObject();
		
		/*shortInfo.put("id_volo", "");
		shortInfo.put("stelle_hotel", 0);
		shortInfo.put("nome_hotel", "");
		shortInfo.put("prezzo_tot", 0);
		shortInfo.put("numeroBambini", 0);
		shortInfo.put("id_hotel", 0);
		shortInfo.put("numeroAdulti", 0);
		shortInfo.put("rating_hotel", "");
		shortInfo.put("immagine_hotel", "");*/
		
		box.put("shortInfo", shortInfo);
		box.put("details", details);
		
		infoViaggio.put("infoViaggio", box);
		
		for(int i=0;i<size;i++)
		arr.put(infoViaggio);
		
		return arr;
		
		
	}
	
JSONObject inizializzaJSON_Riepilogo() throws JSONException {
		
		JSONObject box = new JSONObject();
		JSONObject infoViaggio = new JSONObject();
		JSONObject shortInfo = new JSONObject();
		JSONObject details = new JSONObject();
		
		/*shortInfo.put("id_volo", "");
		shortInfo.put("stelle_hotel", 0);
		shortInfo.put("nome_hotel", "");
		shortInfo.put("prezzo_tot", 0);
		shortInfo.put("numeroBambini", 0);
		shortInfo.put("id_hotel", 0);
		shortInfo.put("numeroAdulti", 0);
		shortInfo.put("rating_hotel", "");
		shortInfo.put("immagine_hotel", "");
		
		details.put("departureDateTime_out","");
		details.put("arrivalDateTime_out","");
		details.put("departureDateTime_in","");
		details.put("arrivalDateTime_in","");
		details.put("nome_esteso_partenza","");
		details.put("disponibilita","");
		details.put("numero_hotel","");
		details.put("luogo_hotel","");
		details.put("check_in","");
		details.put("check_out","");
		details.put("indirizzo","");
		details.put("descrizione","");
		details.put("prezzo_hotel",0);
		details.put("totalPriceAllPassengers",0);*/
		
		box.put("shortInfo", shortInfo);
		box.put("details", details);
		
		infoViaggio.put("infoViaggio", box);
		
		return infoViaggio;
		
		
	}

}