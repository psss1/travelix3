package Service_Layer.Service_Impl;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import Data_Layer.DAOFactory;
import Data_Layer.Prenotazioni.Prenotazione;
import Data_Layer.Prenotazioni.PrenotazioniDAO;
import Service_Layer.IHotel;
import Service_Layer.IPagamento;
import Service_Layer.IPrenotazione;
import Service_Layer.ConnessioneServiziEsterniIMPL.ProxyHotel;
import Service_Layer.ConnessioneServiziEsterniIMPL.ProxyPagamento;

public class PrenotazioneIMPL implements IPrenotazione{

	public boolean creaPrenotazione(String infoPrenotazione, String username) {

		/* Viene creato un oggetto di tipo Prenotazione, usando lo specifico costruttore adatto
		 * alla creazione. Questo oggetto verr� poi passato alla classe DAO per effettuare
		 * la query di creazione (CREATE_QUERY). Se la query andr� a buon fine,
		 * verr� restituito true, altrimenti false.
		 * 
		 * IMPORTANTE, oltre alla creazione della tupla nel DB, questa funzione si mette
		 * in contatto con la tabella hotel (attraverso il relativo proxy) per cambiare la disponibilit�
		 * dell'hotel da "si" a "no".
		 */
		
		boolean esitoCreazione=false;
		
		try {

		DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.PRENOTAZIONI);
		PrenotazioniDAO riverDAO = (PrenotazioniDAO) mysqlFactory.getMYSQLDAO();

		//Recupero i valori dai campi del file JSON
		
		JSONObject riepilogo = new JSONObject(infoPrenotazione);
		JSONObject infoViaggio = riepilogo.getJSONObject("infoViaggio");
		
		
		JSONObject shortInfo = infoViaggio.getJSONObject("shortInfo");
		JSONObject details = infoViaggio.getJSONObject("details");
		
		String luogoOrigine = details.getString("nome_esteso_partenza"); //luogoOrigine
		int prezzoTotale = shortInfo.getInt("prezzo_tot");

		//Recupero prezzoVoli
		
		int prezzoVoli = details.getInt("totalPriceAllPassengers"); //prezzoVoli

		String departureDateTime_out = details.getString("departureDateTime_out");
		String arrivalDateTime_out = details.getString("arrivalDateTime_out");

		//Recupero date e orari del volo

		String departureDateTime_in = details.getString("departureDateTime_in");
		String arrivalDateTime_in = details.getString("arrivalDateTime_in");

		//Passo da questo formato: DateTime = 2020-06-10T16:15:00
		//
		// a questo formato: Date = a questo formato ; Time = 16:15

		String arrDepartureDateTime_out[] = departureDateTime_out.split("T", 2);
		String dataVoloAndataOrigine = arrDepartureDateTime_out[0];                     //DataVoloPartenzaAndata
		String orarioVoloAndataOrigine = arrDepartureDateTime_out[1].substring(0, 5);    //OrarioVoloPartenzaAndata

		String arrArrivalDateTime_out[] = arrivalDateTime_out.split("T", 2);
		String dataVoloAndataDestinazione = arrArrivalDateTime_out[0];                          //DataVoloArrivoAndata
		String orarioVoloAndataDestinazione = arrArrivalDateTime_out[1].substring(0, 5);         //OrarioVoloArrivoAndata

		String arrDepartureDateTime_in[] = departureDateTime_in.split("T", 2);
		String dataVoloRitornoOrigine = arrDepartureDateTime_in[0];                         //DataVoloPartenzaRitorno
		String orarioVoloRitornoOrigine = arrDepartureDateTime_in[1].substring(0, 5);         //OrarioVoloPartenzaRitorno    

		String arrArrivalDateTime_in[] = arrivalDateTime_in.split("T", 2);
		String dataVoloRitornoDestinazione = arrArrivalDateTime_in[0];                             //DataVoloArrivoRitorno
		String orarioVoloRitornoDestinazione = arrArrivalDateTime_in[1].substring(0, 5);         //OrarioVoloPartenzaRitorno

		//Recupero le info dell'hotel
		
		int prezzo_hotel = details.getInt("prezzo_hotel"); 								//prezzoHotel
		String nome_hotel = new String(shortInfo.getString("nome_hotel"));				//nomeHotel
		int stelle_hotel = shortInfo.getInt("stelle_hotel");								//stelleHotel
		String immagine_hotel = new String(shortInfo.getString("immagine_hotel"));		//immagineHotel
		String rating_hotel = new String(shortInfo.getString("rating_hotel"));			//ratingHotel
		String descrizione_hotel = new String(details.getString("descrizione"));	//descrizioneHotel
		String numero_hotel = new String(details.getString("numero_hotel"));			//numeroHotel
		String indirizzo_hotel = new String(details.getString("indirizzo"));		//indirizzoHotel
		String luogo_hotel = new String(details.getString("luogo_hotel"));				//luogoDestinazione
		int id_hotel = shortInfo.getInt("ID_hotel");										//idHotel
		int adulti_hotel = shortInfo.getInt("numeroAdulti");						//numeroAdulti
		int bambini_hotel = shortInfo.getInt("numeroBambini");						//numeroBambini

		/* Al costruttore passiamo i 22 parametri recuperati dal JSON, pi� i 2 ricevuti come param. d'ingresso della funzione.
		 * Gli altri 2 parametri (per arrivare a 26, ovvero il numero di colonne della tabella prenotazione),
		 * saranno settati automaticamente (vedi commento nel costruttore di Prenotazione)
		 */
		
		Prenotazione prenotazione = new Prenotazione(prezzoVoli, prezzo_hotel,  prezzoTotale,
				 orarioVoloAndataOrigine,  orarioVoloAndataDestinazione,  orarioVoloRitornoOrigine,
				 orarioVoloRitornoDestinazione,
				 dataVoloAndataOrigine,  dataVoloAndataDestinazione,  dataVoloRitornoOrigine,
				 dataVoloRitornoDestinazione,
				 adulti_hotel,  bambini_hotel,  nome_hotel,  descrizione_hotel,
				 rating_hotel,  stelle_hotel,  indirizzo_hotel,   immagine_hotel,  luogoOrigine,
				 luogo_hotel,  numero_hotel, username, id_hotel);

		esitoCreazione = riverDAO.createPrenotazione(prenotazione); //chiamo la classe DAO per effettuare la query sul DB (CREATE_QUERY)
		
		if(esitoCreazione) {
			IHotel Ph = new ProxyHotel();
			Ph.cambiaDisponibilitaHotel(id_hotel, "no"); //cambio la disponibilit� dell'hotel prenotato da "si" a "no"
		}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		return esitoCreazione;
	}

	public String visualizzaPrenotazione(int idPrenotazione) {
		
		/* Viene restuita alla servlet una stringa che proviene da un JSON,
		 * il quale ha come campi i valori restituiti dalla query sul DB (READ_QUERY)
		 */
		
		String jsonPrenotazione_str = null;
	
		try {
		
		DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.PRENOTAZIONI);
		PrenotazioniDAO riverDAO = (PrenotazioniDAO) mysqlFactory.getMYSQLDAO();
		
		Prenotazione prenotazione = riverDAO.getPrenotazione(idPrenotazione); //chiamo la classe DAO
																			  //per effettuare la query sul DB (READ_QUERY)
		
		//Costruisco il file JSON
		JSONObject jsonPrenotazione = new JSONObject();
		JSONObject esito = new JSONObject();
		JSONObject item = new JSONObject();
		
		//attraverso le get sull'oggetto prenotazione, setto i campi del file JSON
		item.put("idPrenotazioni", prenotazione.getIdPrenotazione());
		item.put("statoPagamento", prenotazione.getStatoPagamento());
		item.put("prezzoVoli", prenotazione.getPrezzoVoli());
		item.put("prezzoHotel", prenotazione.getPrezzoHotel());
		item.put("prezzoTotale", prenotazione.getPrezzoTotale());
		item.put("orarioVoloAndataOrigine", prenotazione.getOrarioVoloAndataOrigine());
		item.put("orarioVoloAndataDestinazione", prenotazione.getOrarioVoloAndataDestinazione());
		item.put("orarioVoloRitornoOrigine", prenotazione.getOrarioVoloRitornoOrigine());
		item.put("orarioVoloRitornoDestinazione", prenotazione.getOrarioVoloRitornoDestinazione());
		item.put("dataVoloAndataOrigine", prenotazione.getDataVoloAndataOrigine());
		item.put("dataVoloAndataDestinazione", prenotazione.getDataVoloAndataDestinazione());
		item.put("dataVoloRitornoOrigine", prenotazione.getDataVoloRitornoOrigine());
		item.put("dataVoloRitornoDestinazione", prenotazione.getDataVoloRitornoDestinazione());
		item.put("numeroAdulti", prenotazione.getNumeroAdulti());
		item.put("numeroBambini", prenotazione.getNumeroBambini());
		item.put("nomeHotel", prenotazione.getNomeHotel());
		item.put("descrizioneHotel", prenotazione.getDescrizioneHotel());
		item.put("ratingHotel", prenotazione.getRatingHotel());
		item.put("stelleHotel", prenotazione.getStelleHotel());
		item.put("indirizzoHotel", prenotazione.getIndirizzoHotel());
		item.put("immagineHotel", prenotazione.getImmagineHotel());
		item.put("luogoOrigine", prenotazione.getLuogoOrigine());
		item.put("luogoDestinazione", prenotazione.getLuogoDestinazione());
		item.put("numeroHotel", prenotazione.getNumeroHotel());
		item.put("username", prenotazione.getUsername());
		item.put("idHotel", prenotazione.getIdHotel());
		
		jsonPrenotazione.put("infoPrenotazione", item);
		jsonPrenotazione.put("esito", esito);
		
		jsonPrenotazione_str = jsonPrenotazione.toString();
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		return jsonPrenotazione_str;
	}

	public String visualizzaListaPrenotazioni(String username) {
		
		/* Viene restituita alla servlet una stringa che proviene da un JSON,
		 * il quale ha come campi i valori restituiti dalla query sul DB (READ_ALL_QUERY),
		 * simile al caso di visualizzaPrenotazione, solo che stavolta il file JSON conterr�
		 * un JSONArray, avendo a che fare con pi� prenotazioni.
		 */
		
		String jsonListaPrenotazioni_str=null;
		
		try {
		
		DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.PRENOTAZIONI);
		PrenotazioniDAO riverDAO = (PrenotazioniDAO) mysqlFactory.getMYSQLDAO();
		
		List<Prenotazione> prenotazioni = new ArrayList();
		
		prenotazioni = riverDAO.getListaPrenotazioni(username); //chiamo la classe DAO per effettuare la query sul DB (READ_ALL_QUERY)
		
		int size = prenotazioni.size();
		
		Prenotazione box = new Prenotazione();
		
		int[] idPrenotazione_arr = new int[size];
		boolean[] statoPagamento_arr = new boolean[size];
		int[] prezzoTotale_arr = new int[size];
		String[] dataVoloAndataDestinazione_arr = new String[size];
		String[] dataVoloRitornoOrigine_arr = new String[size];
		String[] nomeHotel_arr = new String[size];
		String[] luogoDestinazione_arr = new String[size];
		int[] numeroAdulti_arr = new int[size];
		int[] numeroBambini_arr = new int[size];
		
		
		JSONArray arrayPrenotazioni = new JSONArray();
		JSONObject esito = new JSONObject();
		
		for(int i=0; i<size; i++) {
			
			box = prenotazioni.get(i);
			
			idPrenotazione_arr[i] = box.getIdPrenotazione();
			statoPagamento_arr[i] = box.getStatoPagamento();
			prezzoTotale_arr[i] = box.getPrezzoTotale();
			dataVoloAndataDestinazione_arr[i] = box.getDataVoloAndataDestinazione();
			dataVoloRitornoOrigine_arr[i] = box.getDataVoloRitornoOrigine();
			nomeHotel_arr[i] = box.getNomeHotel();
			luogoDestinazione_arr[i] = box.getLuogoDestinazione();
			numeroAdulti_arr[i] = box.getNumeroAdulti();
			numeroBambini_arr[i] = box.getNumeroBambini();
			
			JSONObject item = new JSONObject();
			JSONObject temp = new JSONObject();
			
			item.put("idPrenotazione", idPrenotazione_arr[i]);
			item.put("statoPagamento", statoPagamento_arr[i]);
			item.put("prezzoTotale", prezzoTotale_arr[i]);
			item.put("dataVoloAndataDestinazione", dataVoloAndataDestinazione_arr[i]);
			item.put("dataVoloRitornoOrigine", dataVoloRitornoOrigine_arr[i]);
			item.put("nomeHotel", nomeHotel_arr[i]);
			item.put("luogoDestinazione", luogoDestinazione_arr[i]);
			item.put("numeroAdulti", numeroAdulti_arr[i]);
			item.put("numeroBambini", numeroBambini_arr[i]);
			item.put("prezzoVoli",0);
			item.put("prezzoHotel",0);
			item.put("orarioVoloAndataOrigine","");
			item.put("orarioVoloAndataDestinazione","");
			item.put("orarioVoloRitornoOrigine","");
			item.put("orarioVoloRitornoDestinazione","");
			item.put("dataVoloAndataOrigine","");
			item.put("dataVoloRitornoDestinazione", "");
			item.put("descrizioneHotel","");
			item.put("ratingHotel","");
			item.put("stelleHotel",0);
			item.put("indirizzoHotel","");
			item.put("immagineHotel","");
			item.put("luogoOrigine","");
			item.put("numeroHotel","");
			item.put("username","");
			item.put("idHotel",0);
			
			temp.put("infoPrenotazione", item);
			temp.put("esito", esito);
			arrayPrenotazioni.put(temp);
			
			System.out.println("arrayPrenotazioni"+i+": "+arrayPrenotazioni.toString());
		}
		
		jsonListaPrenotazioni_str = arrayPrenotazioni.toString();
		
		}catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return jsonListaPrenotazioni_str;
	}

	public boolean pagaPrenotazione(int idPrenotazione) {
	
		/* Viene aggiornato il campo statoPagamento a true, a seguito del pagamento.
		 * Se la query (in tal caso UPDATE_QUERY) va a buon fine, la funzione
		 * restituisce alla servlet true, altrimenti false.
		 * 
		 * Ovviamente, prima di effettuare la modifica sul DB, bisogna effettuare l'operazione di pagamento,
		 * e controllare che essa vada a buon fine. Per fare ci�, viene istanziato un ProxyPagamento.
		 */
		
		boolean esitoPagamento = false; //valore di ritorno della funzione del ProxyPagamento
		boolean esitoUpdate=false;		//valore di ritorno della funzione che fa l'update sul DB delle prenotazioni
		
		IPagamento Pp = new ProxyPagamento();
		
		esitoPagamento = Pp.effettuaPagamento();
		
		if(esitoPagamento) {
			
			DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.PRENOTAZIONI);
			PrenotazioniDAO riverDAO = (PrenotazioniDAO) mysqlFactory.getMYSQLDAO();
			
			esitoUpdate = riverDAO.pagaPrenotazione(idPrenotazione); //chiamo la classe DAO per effettuare la query sul DB (UPDATE_QUERY)
		}
		
		return esitoUpdate;
		
	}

	public boolean eliminaPrenotazione(int idPrenotazione) {
		
		/* Viene eliminata la prenotazione selezionata.
		 * Se la query (in tal caso DELETE_QUERY) va a buon fine, la funzione
		 * restituisce alla servlet true, altrimenti false.
		 * 
		 * Oltre alla creazione della tupla nel DB, questa funzione si mette
		 * in contatto con la tabella hotel (attraverso il relativo proxy) per cambiare la disponibilit�
		 * dell'hotel da "no" a "si". Avendo a disposizione solo l'idPrenotazione, � necessario recuperare
		 * l'idHotel (necessario per cambiare la disponibilit�) facendo una get (READ_QUERY).
		 */
		
		boolean esitoCancellazione = false;

		DAOFactory mysqlFactory = DAOFactory.getDAOFactory(DAOFactory.PRENOTAZIONI);
		PrenotazioniDAO riverDAO = (PrenotazioniDAO) mysqlFactory.getMYSQLDAO();
		
		Prenotazione prenotazione = null;
		
		prenotazione = riverDAO.getPrenotazione(idPrenotazione);
		
		esitoCancellazione = riverDAO.deletePrenotazione(idPrenotazione); //chiamo la classe DAO per effettuare la query sul DB (DELETE_QUERY)
		
		if(esitoCancellazione) {
			IHotel Ph = new ProxyHotel();
			Ph.cambiaDisponibilitaHotel(prenotazione.getIdHotel(), "si"); //cambio la disponibilit� dell'hotel prenotato da "no" a "si"
		}

		return esitoCancellazione;
	}

}