package Web_Layer.User.Controller;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import Service_Layer.IPrenotazione;
import Service_Layer.Service_Impl.PrenotazioneIMPL;

/**
 * Servlet implementation class PrenotazioneController
 */
@WebServlet("/PrenotazioneController")
public class PrenotazioneController extends HttpServlet {
	
	String JSONRisposta(boolean esito) throws JSONException {
		
		/* Funzione che restituisce una stringa, la quale proviene da un JSON con campo "esito",
		 * il quale sar� true/false a seconda del booleano in ingresso.
		 * Da usare nelle funzioni legate a createPrenotazione, pagaPrenotazione
		 * e deletePrenotazione, che restituiscono un booleano
		 */
		
		JSONObject esito_json = new JSONObject();
		JSONObject infoPrenotazione = new JSONObject();
		JSONObject box = new JSONObject();
		
		
		if(esito)
			esito_json.put("esito", true);
			
		else
			esito_json.put("esito", false);
		
		box.put("esito",esito_json);
		box.put("infoPrenotazione", infoPrenotazione);
		
		String esito_str = new String(box.toString());
		
		return esito_str;
		
	}
	
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public PrenotazioneController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		String method = request.getMethod();
		
		if(method.equals("DELETE"))
			doDelete(request,response);
		
		else if(method.equals("PUT"))
			doPut(request,response);
		
		else if(method.equals("POST"))
			doPost(request,response);
		
		else if(method.equals("GET")){
			
			String id_prenotazione_str = new String ();
			
			id_prenotazione_str = request.getParameter("idPrenotazione");
			
			if(id_prenotazione_str==null) 
				doGet(request,response);

			else{
				
				System.out.println("ciao");
				
				int id_prenotazione = Integer.parseInt(id_prenotazione_str);
				
				doGet(request,response,id_prenotazione);

			}
		}
	}

	//------------Get per recuperare tutte le prenotazioni di un utente---------------------------------------------
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		HttpSession sessione = request.getSession();
		
		String username = (String) sessione.getAttribute("username"); //da prendere nella sessione
		
		IPrenotazione prenotazioneIMPL = new PrenotazioneIMPL();
		
		String jsonPrenotazioni = prenotazioneIMPL.visualizzaListaPrenotazioni(username);
		
		System.out.print("Get All prenotazioni:"+jsonPrenotazioni);
		
		response.setHeader("jsonPrenotazioni", jsonPrenotazioni);
		
		request.getRequestDispatcher( "/pagine/prenotazione.jsp" ).forward(request,response);
	}
	
	//--------------Get per recuperare una singola prenotazione--------------------------------------------------------
	protected void doGet(HttpServletRequest request, HttpServletResponse response, int idPrenotazione) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		IPrenotazione prenotazioneIMPL = new PrenotazioneIMPL();
		
		String JSONPrenotazione = prenotazioneIMPL.visualizzaPrenotazione(idPrenotazione);
		
		System.out.println("Get singola prenotazione: "+ JSONPrenotazione);
		
		response.setContentType("application/json");
		
		PrintWriter out = response.getWriter();
		
		out.write(JSONPrenotazione);
		
		out.flush();
		out.close();
	}

	//---------------Post per creare la prenotazione----------------------------
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		
		try {
		
		/*All'interno del DB la tabella prenotazione ha 26 colonne:
		 * 
		 * -IdPrenotazione non va creato qui, quando viene fatta una insert sul DB,
		 *  viene inserito automaticamente (autoincrement)
		 *  
		 *  -statoPagamento al momento della creazione viene settato automaticamente a false,
		 *  sfruttando il costruttore apposito della classe Prenotazione
		 *  
		 *  -Username viene preso dalla request
		 *  -prezzoTotale viene preso dalla request
		 *  
		 *  -I restanti 22 parametri sono racchiusi nel JSON "JSONRiepilogo",
		 *  che viene preso dalla request. L'estrazione di questi parametri dal JSON
		 *  � affidata a prenotazioneIMPL, istanza della classe PrenotazioneIMPL, attributo della servlet (riga 24)
		 */
			
		HttpSession sessione = request.getSession();
		
		String username = (String) sessione.getAttribute("username");
		String JSONRiepilogo = request.getParameter("JSONRiepilogo");
		
		IPrenotazione prenotazioneIMPL = new PrenotazioneIMPL();
		
		boolean esitoCreazione = prenotazioneIMPL.creaPrenotazione(JSONRiepilogo, username);
		
		String esito_str = new String(JSONRisposta(esitoCreazione));
		
		System.out.println("Post prenotazione : "+esito_str);
		
		response.setContentType("application/json");
		PrintWriter out = response.getWriter();
		out.write(esito_str);
		out.flush();
		out.close();
		
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

	//-------------Put per cambiare lo statoPagamento della prenotazione, da usare quando viene effettuato il pagamento-------------
	protected void doPut(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
		
		String idPrenotazione_str = request.getParameter("idPrenotazione");
		int idPrenotazione = Integer.parseInt(idPrenotazione_str);
		
		IPrenotazione prenotazioneIMPL = new PrenotazioneIMPL();
		
		boolean esitoPagamento = prenotazioneIMPL.pagaPrenotazione(idPrenotazione);
		
		String esito_str = new String(JSONRisposta(esitoPagamento));
		
		System.out.print("Put prenotazioni:"+esito_str);
		
		response.setContentType("application/json");
		
		PrintWriter out = response.getWriter();
		
		out.write(esito_str);
		
		out.flush();
		out.close();
		}
		
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	//--------------Delete per eliminare la prenotazione-------------------------
	protected void doDelete(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		try {
		
		String idPrenotazione_str = request.getParameter("idPrenotazione");
		int idPrenotazione = Integer.parseInt(idPrenotazione_str);
		
		IPrenotazione prenotazioneIMPL = new PrenotazioneIMPL();
		
		boolean esitoCancellazione = prenotazioneIMPL.eliminaPrenotazione(idPrenotazione);
		
		String esito_str = new String(JSONRisposta(esitoCancellazione));
		
		System.out.println("Delete prenotazioni: "+esito_str);
		
		response.setContentType("application/json");
		
		PrintWriter out = response.getWriter();
		
		out.write(esito_str);
		
		out.flush();
		out.close();
		}
		
		catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}

}
