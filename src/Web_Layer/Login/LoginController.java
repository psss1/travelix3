package Web_Layer.Login;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.json.JSONException;
import org.json.JSONObject;

import Service_Layer.ILogin;
import Service_Layer.Service_Impl.LoginIMPL;

/**
 * Servlet implementation class LoginController
 */
@WebServlet("/LoginController")
public class LoginController extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#HttpServlet()
	 */
	public LoginController() {
		super();
		// TODO Auto-generated constructor stub
	}

	/**
	 * @see HttpServlet#service(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void service(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub

		String method = request.getMethod();
		if (method.equals("GET")) {
			doGet(request, response);
		}

		else if(method.equals("POST"))
			doPost(request, response);
	}

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String nomefb = request.getParameter("name");
		String cognome = request.getParameter("surname");
		String token = request.getParameter("token");
		boolean esito = false;

		ILogin facebook = new LoginIMPL();

		esito = facebook.accediFacebook(token);

		HttpSession sessione = request.getSession();

		if (esito) {

			sessione.setAttribute("username", nomefb+"_"+cognome);
			sessione.setAttribute("nome", nomefb);
			sessione.setAttribute("ruolo", "user");

			request.getRequestDispatcher( "/pagine/home.jsp" ).forward(request,response);
		}
		else{
			request.getRequestDispatcher( "/login.jsp" ).forward(request,response);
		}


	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		//NOTA: in mancanza di una procedura di registrazione ho usato le seguenti 
		//istruzioni per generare gli hash delle password da mettere nel db
		//al posto delle password in chiaro; aggiungendo la procedura di registrazione
		//le password verrebbero trasformate dopo che l'utente le ha inserite in un form

		//String userpw = DigestUtils.sha256Hex("user");
		//String adminpw = DigestUtils.sha256Hex("admin");

		try {

			//parametri presi dal form di login:
			String username = request.getParameter("username");
			String password = request.getParameter("password");

			//calcolo l'hash per poterlo confrontare con il contenuto
			//del db:

			ILogin accesso = new LoginIMPL();
			
			String JSONUtente_str = accesso.accedi(username,password);

			JSONObject JSONUtente = new JSONObject(JSONUtente_str);

			String esito = JSONUtente.getString("esito");

			if(esito.equals("true")){    
				HttpSession sessione = request.getSession();

				String nome = JSONUtente.getString("nome");
				String ruolo = JSONUtente.getString("ruolo");

				sessione.setAttribute("username", username);
				sessione.setAttribute("nome", nome);
				sessione.setAttribute("ruolo", ruolo);

				if (ruolo.equals("user")) {
					request.getRequestDispatcher( "/pagine/home.jsp" ).forward(request,response);
				}
				else if(ruolo.equals("admin"))
					request.getRequestDispatcher( "/riservata/riservata.jsp" ).forward(request,response);
			} 
			else{
				HttpSession sessione = request.getSession();
				sessione.setAttribute("error","Login failed. Retry.");
				request.getRequestDispatcher( "/login.jsp" ).forward(request,response);

			}
		} catch (JSONException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
