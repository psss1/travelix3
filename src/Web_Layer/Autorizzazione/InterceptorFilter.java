/*
 * Dico prenditi la sessione, vedi se c'� un utente in sessione, se non c'� nessun utente, vuol dire che io volevo accedere a queste risorse,
 * chi � che mi dice quali risorse fanno attivare questo filtro? il web.xml.
 * Nel web.xmp c'� il mapping dei filtri.
 * Interceptor filtra tutte le chimate del tipo pagine, se volessi accedere a panine/home, entra in gioco il filtro.
 * Vede se c'� un utente in sessione, se non c'� rimanda alla login, finch� non ci sar� l'utente in sessione
*/

package Web_Layer.Autorizzazione;

import java.io.IOException;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.*;

/**
 * Servlet Filter implementation class AuthFilter
 */
public class InterceptorFilter implements Filter {
	

    /**
     * Default constructor. 
     */
    public InterceptorFilter() {
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see Filter#destroy()
	 */
	public void destroy() {
		// TODO Auto-generated method stub
	}

	/**
	 * @see Filter#doFilter(ServletRequest, ServletResponse, FilterChain)
	 */
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException 
	{
		HttpServletRequest req = (HttpServletRequest) request;
		HttpServletResponse res = (HttpServletResponse) response;
    
		HttpSession session = req.getSession();
        if(session.getAttribute("username")==null)
        	res.sendRedirect(req.getContextPath()+"/login.jsp");
        else      
        	//vai dove volevi andare
        	chain.doFilter(request, response);
	        
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}

	
}
