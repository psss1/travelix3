package Data_Layer.Prenotazioni;

import java.util.List;

import Data_Layer.MYSQLDAO;

public interface PrenotazioniDAO extends MYSQLDAO{
	
	public Prenotazione getPrenotazione(int id_prenotazione);
	 
	public List<Prenotazione> getListaPrenotazioni(String username);
	
	public boolean createPrenotazione(Prenotazione prenotazione);
	
	public boolean pagaPrenotazione(int idPrenotazione);
	
	public boolean deletePrenotazione(int idPrenotazione);
	
	
}