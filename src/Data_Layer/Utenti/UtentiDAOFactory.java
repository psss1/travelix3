package Data_Layer.Utenti;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import Data_Layer.DAOFactory;

public class UtentiDAOFactory extends DAOFactory{
	
    public static final String DRIVER = "com.mysql.cj.jdbc.Driver";
    public static final String DBURL = "jdbc:mysql://utentidb.ckzl6dcqth1d.eu-central-1.rds.amazonaws.com:3308/utentidb";
    
    public static final String USER = "admin";
    public static final String PASS = "Travelix2020";
    
    public static Connection createConnection() {
        Connection conn = null;
        try {
            Class.forName(DRIVER);
            conn = (Connection) DriverManager.getConnection(DBURL, USER, PASS);
            conn.setAutoCommit(true);
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        	e.printStackTrace();
        }
        return conn;
    }
    
	@Override
	public UtentiDAO getMYSQLDAO() {
		return new UtentiDAOImpl();
	}

}
