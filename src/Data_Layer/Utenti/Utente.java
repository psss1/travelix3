package Data_Layer.Utenti;

public class Utente {
	
	private String username;
	private String password;
	private String ruolo;
	private String nome;
		
	public Utente(String password, String nome, String ruolo) {
		super();
		this.password = password;
		this.nome = nome;
		this.ruolo = ruolo;
	}
 
	public String getUsername() {
		return username;
	}
 
	public void setUsername(String username) {
		this.username = username;
	}
	
	public String getNome() {
		return nome;
	}
 
	
	public void setNome(String nome) {
		this.nome = nome;
	}
 
	public String getPassword() {
		return password;
	}
 
	public void setPassword(String password) {
		this.password = password;
	}
 
	public String getRuolo() {
		return ruolo;
	}
 
	public void setRuolo(String ruolo) {
		this.ruolo = ruolo;
	}
	
}
