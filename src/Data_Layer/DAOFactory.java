package Data_Layer;

import Data_Layer.Prenotazioni.PrenotazioniDAOFactory;
import Data_Layer.Utenti.UtentiDAOFactory;

public abstract class DAOFactory {
	
    public static final int UTENTI = 0;  
    public static final int PRENOTAZIONI = 1;
 
    public abstract MYSQLDAO getMYSQLDAO();
    
    public static DAOFactory getDAOFactory(int database) {
        switch (database) {
        case UTENTI:
            return new UtentiDAOFactory();
        case PRENOTAZIONI:
             return new PrenotazioniDAOFactory();
        default:
            return null;
        }
    }

}
