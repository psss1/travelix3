# TravelixPSSS

####Ros1
l'admin non vede la home ma solo la pagina riservata, l'user sia di fb che normale vede solo la home 
####Rosa2
ho un solo pulsante di logout sia per facebook sia per l'user
####Rosa3
ho aggiunto departure nella home, ho mesos una sola barra invece di 2 (hotel + flight), ho aggiunto adulti e bambini e modificato i css per estetica
####Rosa4
ho eliminato homeFB e riservata2 riservata3. Non servivano
####Rosa5
inserimento servlet di ricerca
####Rosa6
ho creato una risposta da mandare alla pagina risultati
####Rosa7
aggiunta di librerie lib
####Rosa8
riesce a passare il prezzo alla pagina risultati
####Rosa9
ho inserito una sorta di abbellimento ma ha bisogno di aggiustamenti
####Rosa10
aggiustato il formato della data
####Rosa11
ATTENZIONE! versione non molto funzionante, al token piace cambiare. Bisogna trovare un modo per togliere il token di sessione o un modo per farlo generare da DreamFactory
####Rosa12
create le funzioni di GET e PUT degli hotel
####Rosa13
sistemata la parte estetica della pagina risultati, creata la pagina booking ma non ha collegamenti
####Rosa14
generalizzato le funzioni (voli, get, put) in Ricerca. Men� a tendina delle citt� con divisione, 
ovvero, ho sia nome della citt� che IATA code, successivamente li divido per darlo agli hotel e voli. 
Output del json messi in array. Aggiunta cartella con immagini hotel.
####Giuseppe1
commit del file risultati.jsp -> script con loop div 
####Giuseppe2
script con loop div funziona ma non arrivano i valori corretti 
####Giuseppe3
script con loop div funziona, arrivano i valori corretti. risolvere problema stelle e immagini
####Rosa15
div loop con TUTTE le informazioni, stiamo cercando di far uscire un popup in caso di errore di ricerca, per il momento l'utente viene reindirizzato alla home
####Rosa16
esce il popup con l'errore. Inserita servlet per il riepilogo (funziona). creata una classe Proxy sia per voli che per hotel. finire Booking
####Giuseppe4
creato ID_volo e passato a booking
####Checco1
rifatta la query sul volo nella pagina booking, passati i parametri alla relativa pagina javascript
####Checco2
aggiunti commenti al codice, dubbi sull'errore ricerca
####Checco3
Servlet prenotazione, se si clicca "book" in Booking viene cambiata la disponibilit� di quell'hotel a no, sfruttando la funzione "elimina_hotel" di ProxyHotel; riempita booking.jsp sfruttando gli stub di booking2.jsp
In Ricerca, il prezzo � adesso moltiplicato per il numero di notti (e sommato a quello dell'aereo)
####Checco4
ProxyHotel "elimina_hotel" generalizzata in "cambia_disponibilita_hotel". Bisogna ragionare su dove posizionare "Prenotazione", con quali entit� deve interagire e come. Creata la nuova tabella "prenotazioni" sul db, che Dreamfactory non riesce a leggere 
####Checco5
Creati nuovi package e classi per rispecchiare il diagramma architetturale
####Checco6
Altre modifiche al package Prenotazioni
####Rosa17
sistemata la schermata di riepilogo. Aggiunto lo stub del volo*, Mancano ancora altre cose dell'hotel. Ci sta il tasto home a caso per� sticazzi
####Rosa18
dovrebbe esser completa la pagina di riepilogo. Aggiunto il tasto home per ritornare alla home, c'� anche quello di prenotazioni ma per ora � uno stub. Non puoi inserire una data prima del giorno odierno (home)
####Rosa19
creato un database su aws per gli utenti
####Checco7
creato database per le prenotazioni su aws, connessione gestita tramite Dreamfactory, implementati stub di prova per fare POST, PUT e DELETE
####Rosa20
corretto errore quando non si trovano hotel
####Rosa21
Sistemata la home e creata la pagina di prenotazione. Problemi: una volta ritornata dalla oagina di prenotazione, non riesco pi� ad effettuare una ricerca (si invalida il pulsante, probabilmente dipende da ajax ma non so come funziona). Ancora non � stato corretto il popup di errore ma credo sia lo stesso problema prima citato. Sistemare la pagina di riepilogo che � sfasata 
####Rosa22 
corretto il login con facebook e funziona anche il logout
####Checco8
sistemato popup errore ricerca
####Checco9
Riempito ProxyFacebook e accediFacebook (modificare diagrammi)
####Checco10
Modificato invio dell'username nella sessione (Per facebook "Nome_Cognome")
####Checco11
Prezzo totale viene preso dal file JSON e non come parametro hidden; modificati i parametri da visualizzare nelle prenotazioni compatte
####Checco12
Spostati gli script di home in file a parte.
ERRORE CLASSPATH? Properties -> Java Build Path -> Classpath -> Add Library -> Server runtime (?) -> Apache (apply...)
####Rosa23
Aggiunto script logout da prenotazione e aggiunte le colonne Adulti, Bambini.
####Checco13
Errore doppia ricerca (tornare indietro) risolto solo con l'accesso tramite Facebook 
####Checco14
Implementata funzione "visualizzaListaPrenotazioni", anche nella pagina; tentativo di implementare "elimina prenotazione", la servlet non riesce a prendere il comando
####Checco15
Implementate parzialmente tutte le funzioni di prenotazione. Errori da correggere:

-Nell'ajax, settando un type diverso da "POST" o "GET" (ad esempio "DELETE"), la servlet non riesce a prendere gli input (comando, idPrenotazione)
-mostraPrenotazione/visualizzaDettagli, mancano gli stili, va rimosso il bottone book

####Checco16
-Cambiata la funzione service della servlet PrenotazioneController:
viene fatto primo un controllo sul "method" della servlet, se esso corrisponde a POST, PUT o DELETE vengono avviate le relative funzioni, se corrisponde a GET viene fatto un controllo su "comando" per decidere quale delle 2 get avviare
-statoPagamento, in corrispondenza di true o false, vengono stampate delle icone corrispondenti (checkmark verde e X rossa, disponibili anche in viola)

####Checco17
Modificata la head di "risultati" (catalogo)

####Checco18
Modificate le head delle pagine
Bisogna risolvere il problema del redirect alla home, fatto questo si deve aggiungere il tasto home/prenotazioni dove manca

####Rosa24
sistemato il problema del redirect che non effettuava la ricerca. Inserito il pulsante home

####Checco19
In "Prenotazioni", il bottone "paga" viene mostrato solo se la prenotazione non � stata pagata, altrimenti stampa "Pagamento effettuato"

####Checco20
Modificata funzione "accedi(...)" in Accesso e relativo uso della servlet LoginController

####Rosa25
Risolti i seguenti problemi: 
-Nessun risultato trovato -> errore tomcat, no, dovrebbe ricaricare la pagina
-Dopo aver creato la prenotazione l'utente deve essere reindirizzato alla home (anche in caso di prenotazione fallita)

####Rosa26
eliminata la cartella degli script dalla sottocartella pagine

####Checco22
Cambiata struttura delle DAO, e delle altri classi (coerenza con diagramma architetturale)

####Rosa27
Aggiustato il caricamento del login

####Checco23
Cambiati alcuni nomi di variabili in RicercaIMPL (coerenza diagrammi-codice, in particolare sequence diagram); aggiunto script in fondo a prenotazione.jsp

####Checco24

Coerenza diagrammi-codice, data layer

####Checco25

Rimosso un alert da scriptPrenotazione.js

####Checco26

Cambiati gli url quando si va avanti con la ricerca (da cambiare quello del pulsante prenotazione)

####Checco27

Cambiato URL della lista prenotazioni ("PRENOTAZIONI"), eliminato il funzionamento delle servlet tramite "comando" (tolto poich� usciva negli URL); le risorse "viaggio" e "prenotazione" adesso sono protette dall'InterceptorFilter. Da sistemare il logout con Facebook (cercare "???"), gli script di Facebook nelle pagine vanno messi in "scriptFacebook.js"

####Checco28

Si devono modificare gli alert, e la larghezza delle colonne della tabella prenotazioni (<th style "width:"...)