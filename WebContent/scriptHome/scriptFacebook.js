function facebookLogout(){
	FB.getLoginStatus(function(response) {
		  if (response.status === 'connected') {
			  FB.logout(function(response) {
				  window.location.href = "/TravelixCopia/logout.jsp";
				  });
			  }
		  else
			  window.location.href = "/TravelixCopia/logout.jsp"; 
		  });
}

window.fbAsyncInit = function() {
	FB.init({
		appId      : '1230843143781356',
		cookie     : true,                     // Enable cookies to allow the server to access the session.
		xfbml      : true,                     // Parse social plugins on this webpage.
		version    : 'v5.0'           // Use this Graph API version for this call.
	});
}

(function(d, s, id) {                      // Load the SDK asynchronously
	var js, fjs = d.getElementsByTagName(s)[0];
	if (d.getElementById(id)) return;
	js = d.createElement(s); js.id = id;
	js.src = "https://connect.facebook.net/en_US/sdk.js";
	fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));