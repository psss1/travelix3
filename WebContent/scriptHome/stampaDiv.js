function viaggi(immagine_hotel, nome_hotel, ID_hotel,  ID_volo, stelle_hotel, rating_hotel, prezzo_tot, numeroAdulti, numeroBambini ,i){
	
	let persone = numeroAdulti+numeroBambini;
	
	var catalogo_viaggio= 
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					'</br>'+
					   '<div class="container">'+
					    '<div class="row">'+
					    '<div class="col-lg-1 temp_col"></div>'+
					    '<div class="col-lg-11">'+
					    '<!-- Offers Sorting -->'+
					    '</div>'+
					    '<div class="col-lg-12">'+
					'<div class="offers_item rating_4">'+
					'<form id="search_form_riepilogo'+i+'" name="search_form_riepilogo'+i+'" method="get"> '+
					'<div class="row"> ' +
					'<div class="col-lg-1 temp_col"></div>'+
					'<div class="col-lg-3 col-1680-4">'+
						'<div class="offers_image_container">'+
							'<div class="offers_image_background" style="background-image:url(pagine/immagini_hotel/'+immagine_hotel+')"></div>'+
						'</div>'+
					'</div>'+
					'<div class="col-lg-8">'+
						'<div class="offers_content">'+
							'<div class="offers_price">'+nome_hotel+'   </div>'+
							'<input type="hidden" id="ID_hotel'+i+'" name="ID_hotel'+i+'" value="'+ID_hotel+'" />'+
							'<input type="hidden" id="ID_volo'+i+'" name="ID_volo'+i+'" value="'+ID_volo+'" />'+
							'<div class="rating_r rating_r_'+stelle_hotel+' offers_rating" data-rating="'+stelle_hotel+'">'+
								'<i></i>'+
								'<i></i>'+
								'<i></i>'+
								'<i></i>'+
								'<i></i>'+
							'</div>'+
							'<p class="offers_text"></p>'+
							'<div class="offers_icons">'+
								'<ul class="offers_icons_list">'+
								'</ul>'+
							'</div>'+
							'<button class="button search_button" id="bottone_riepilogo">Visualizza Dettagli<span></span><span></span><span></span></button>'+
							'<div class="offer_reviews">'+
								'<div class="offer_reviews_content">'+
									'<div class="offer_reviews_title">rating</div>'+
								'</div>'+
								'<div class="offer_reviews_rating text-center">'+rating_hotel+'</div>'+
								'</br>'+
								'</br>'+
								'</br>'+
								'<div class="offers_price">'+prezzo_tot+'  Euro<span></br> volo + hotel (per '+persone+' persone)</span></div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
				'</div>'+
				'</div>'+
				'</form>'+
			'</div>'+
			'</br>'+
			'</br>';
	
	return catalogo_viaggio;
}


function riepilogoViaggio(nome, stelle, indirizzo, numero, prezzo_totale, immagine, descrizione, persone, prezzo, 
							prezzoVolo, immagine_camera, camera, departureTime_out, departureDate_out, nome_esteso_partenza, arrivalTime_out, 
							arrivalDate_out,luogo, departureTime_in,departureDate_in,arrivalTime_in,arrivalDate_in,bottone){
	
	let stampa_bottone = "";
	
	if(bottone){
		stampa_bottone='<button class="button search_button">Prenota<span></span><span></span><span></span></button>';
	}
	
	var riep_viaggio=
		
			'<div class="container">'+
			'</br>'+
			'</br>'+
			'</br>'+
			'</br>'+
			'</br>'+
			'</br>'+
				'<div class="row">'+
			'<div class="col-lg-12">'+
				'<div class="single_listing">'+
					
					'<!-- Hotel Info -->'+
		'<form id="search_form_prenotazione" method="post">'+
					'<div class="hotel_info">'+
						'<!-- Title -->'+
						'<div class="hotel_title_container d-flex flex-lg-row flex-column">'+
							'<div class="hotel_title_content">'+
								'<h1 class="hotel_title">'+nome+'</h1>'+
								'<div class="rating_r rating_r_'+stelle+' hotel_rating">'+
									'<i></i>'+
									'<i></i>'+
									'<i></i>'+
									'<i></i>'+
									'<i></i>'+
								'</div>'+
								'<div class="hotel_location">'+indirizzo+'</div>'+
								'<div class="hotel_location">Tel. '+numero+'</div>'+
							'</div>'+	
							'<div class="hotel_title_button ml-lg-auto text-lg-right">'+
								'<div class="offers_price">'+prezzo_totale+' Euro &emsp;&ensp;</div>'+
								stampa_bottone+
							'</div>'+
						'</div>'+

						'<!-- Listing Image -->'+

						'<div class="hotel_image">'+
							'<img src="pagine/immagini_hotel/'+immagine+'" alt="">'+
						'</div>'+	

						'<!-- Hotel Info Text -->'+
						
						'<div class="hotel_info_text">'+
							'<h1 class="hotel_title">Descrizione</h1>'+
							'<p>'+descrizione+'</p>'+
						'</div>'+
						
						'<!-- Info Prezzi -->'+
						'<div class="hotel_info_text">'+
							'<h1 class="hotel_title">Info Prezzi</h1>'+
							'<div class="room_title">Prezzo hotel per '+persone+' persone: &ensp;'+prezzo+' euro</div>'+
							'</br>'+
							'<div class="room_title">Prezzo volo per '+persone+' persone (andata e ritorno): &emsp;'+prezzoVolo+' euro</div>'+
						'</div>'+
						

						'<!-- Hotel Info Tags -->'+

						'<div class="hotel_info_tags">'+
							'<ul class="hotel_icons_list">'+
								'<li class="hotel_icons_item"><img src="images/post.png" alt=""></li>'+
								'<li class="hotel_icons_item"><img src="images/compass.png" alt=""></li>'+
								'<li class="hotel_icons_item"><img src="images/bicycle.png" alt=""></li>'+
								'<li class="hotel_icons_item"><img src="images/sailboat.png" alt=""></li>'+
							'</ul>'+
						'</div>'+

					'</div>'+
					'</form>'+
					
					'<!-- Rooms -->'+

					'<div class="rooms">'+

						'<!-- Room -->'+
						'<div class="room">'+

							'<!-- Room -->'+
							'<div class="row">'+
								'<div class="col-lg-2">'+
									'<div class="room_image"><img src='+immagine_camera+' alt="https://unsplash.com/@grovemade"></div>'+
								'</div>'+
								'<div class="col-lg-7">'+
									'<div class="room_content">'+
										'<div class="room_title">Camera '+camera+'</div>'+
										'<div class="room_price">'+prezzo+'/notte</div>'+

									'</div>'+
								'</div>'+
								'<div class="col-lg-3 text-lg-right">'+
									'<div class="room_button">'+
									
									'</div>'+
								'</div>'+
							'</div>'+	
						'</div>'+
					'</div>'+
					
'<div class="stats">'+
	'<div class="container">'+
		'<div class="row">'+
			'<div class="col text-center">'+
				'<div class="section_title">FLIGHT</div>'+
			'</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col-lg-10 offset-lg-1 text-center">'+
				'<p class="stats_text"> </p>'+
			'</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col">'+
				'<div class="stats_years">'+
					'<div class="stats_years_last">&emsp; &emsp;Departure</div>'+
					'<div class="stats_years_new float-right">Destination</div>'+
				
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col">'+
				'<div class="stats_contents">'+
					
					'<!-- Stats Item -->'+
					'<div class="stats_item d-flex flex-md-row flex-column clearfix">'+
						'<div class="stats_last order-md-1 order-3">'+
							'<div class="stats_last_icon d-flex flex-column align-items-center justify-content-end">'+
								'<img src="images/icon_2.png" alt="">'+
							'</div>'+
							'<div class="stats_last_content">'+
								'<div class="stats_number">&ensp; &ensp;'+departureTime_out+'</div>'+
								'<div class="stats_type">&emsp; &emsp;'+departureDate_out+'</div>'+
								'<div class="stats_type">&emsp; &emsp;'+nome_esteso_partenza+'</div>'+
							'</div>'+
						'</div>'+
						'<div class="stats_bar order-md-2 order-2" data-x="1642" data-y="3527" data-color="#31124b">'+
						'</div>'+
						'<div class="stats_new order-md-3 order-1 text-right">'+

							'<div class="stats_new_content">'+
								'<div class="stats_number">'+arrivalTime_out+'</div>'+
								'<div class="stats_type">'+arrivalDate_out+'</div>'+
								'<div class="stats_type">'+luogo+'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+
		
		'<div class="row">'+
			'<div class="col">'+
				'<div class="stats_years">'+
				
				'</div>'+
			'</div>'+
		'</div>'+
		'<div class="row">'+
			'<div class="col">'+
				'<div class="stats_contents">'+
					
					'<!-- Stats Item -->'+
					'<div class="stats_item d-flex flex-md-row flex-column clearfix">'+
						'<div class="stats_last order-md-1 order-3">'+
							'<div class="stats_last_icon d-flex flex-column align-items-center justify-content-end">'+
								'<img src="images/icon_2_riflessa.png" alt="">'+
							'</div>'+
							'<div class="stats_new order-md-3 order-1 text-left"></div>'+
							'<div class="stats_last_content">'+
								'<div class="stats_number">&ensp; &ensp;'+departureTime_in+'</div>'+
								'<div class="stats_type">&emsp; &emsp;'+departureDate_in+'</div>'+
								'<div class="stats_type">&emsp; &emsp;'+luogo+'</div>'+
							'</div>'+
						'</div>'+

						'<div class="stats_bar order-md-2 order-2" data-x="1642" data-y="3527" data-color="#31124b">'+
						'</div>'+
						'<div class="stats_new order-md-3 order-1 text-right">'+
							'<div class="stats_new_content">'+
								'<div class="stats_number">'+arrivalTime_in+'</div>'+
								'<div class="stats_type">'+arrivalDate_in+'</div>'+
								'<div class="stats_type">'+nome_esteso_partenza+'</div>'+
							'</div>'+
						'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>'+

		
	'</div>'+
'</div>'+
					'</div>'+
				'</div>'+
			'</div>'+
		'</div>';
	
	
	
	return riep_viaggio;
	
}