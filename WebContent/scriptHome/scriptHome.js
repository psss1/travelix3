function effettua_prenotazione(box){

	$.ajax({
		type:'POST',
		url:'prenotazione',
		data: {JSONRiepilogo: box},
		dataType: 'json',   
		success:function(esitoCreazione){

			let esito = esitoCreazione.esito.esito;

			if(esito){
				alert("Prenotazione avvenuta con successo! Puoi controllare la lista delle tue " +
						"prenotazioni cliccando sul bottone PRENOTAZIONI.");
			}

			else{
				alert("Errore prenotazione");
			}
			
			window.location.href = "https://localhost:8443/TravelixCopia/pagine/home.jsp";
			
		}

	});	

}

function visualizzaRiepilogo(i){

	let ID_hotel = $('#ID_hotel'+i).val();
	let ID_volo = $('#ID_volo'+i).val();

	$.ajax({
		type:'GET',
		url:'viaggio',
		data: {ID_hotel: ID_hotel, ID_volo: ID_volo},
		dataType: 'json',   
		success:function(json_viaggio){

			let contenuto;

			var box = JSON.stringify(json_viaggio);
			
			let shortInfo = json_viaggio.infoViaggio.shortInfo;
			let details = json_viaggio.infoViaggio.details;

			let nome_esteso_partenza = details.nome_esteso_partenza;
			let prezzo_totale = shortInfo.prezzo_tot;
			let prezzoVolo = details.totalPriceAllPassengers;

			//OUTBOUND

			let departureDateTime_out = details.departureDateTime_out;
			let arrivalDateTime_out = details.arrivalDateTime_out;

			let arrDepartureDateTime_out = departureDateTime_out.split("T",2);
			let departureDate_out = arrDepartureDateTime_out[0];
			let departureTime_out = arrDepartureDateTime_out[1].substring(0, 5);

			let arrArrivalDateTime_out = arrivalDateTime_out.split("T",2);
			let arrivalDate_out = arrArrivalDateTime_out[0];
			let arrivalTime_out = arrArrivalDateTime_out[1].substring(0, 5);

			//INBOUND

			let departureDateTime_in = details.departureDateTime_in;
			let arrivalDateTime_in = details.arrivalDateTime_in;

			let arrDepartureDateTime_in = departureDateTime_in.split("T",2);
			let departureDate_in = arrDepartureDateTime_in[0];
			let departureTime_in = arrDepartureDateTime_in[1].substring(0, 5);

			let arrArrivalDateTime_in = arrivalDateTime_in.split("T",2);
			let arrivalDate_in = arrArrivalDateTime_in[0];
			let arrivalTime_in = arrArrivalDateTime_in[1].substring(0, 5);

			let camera; //Camera doppia/quadrupla
			let immagine_camera; //Camera doppia/quadrupla
			
			let persone = shortInfo.numeroAdulti + shortInfo.numeroBambini;

			if(persone){
				camera="Doppia";
				immagine_camera = "images/doppia.jpg";
			}
			else{
				camera="Quadrupla";
				immagine_camera = "images/quadrupla.jpg";
			}
			
			let bottone = true;

			contenuto = riepilogoViaggio(shortInfo.nome_hotel, shortInfo.stelle_hotel, details.indirizzo, details.numero_hotel, prezzo_totale, 
					shortInfo.immagine_hotel, details.descrizione, persone, details.prezzo_hotel, prezzoVolo, immagine_camera, 
					camera, departureTime_out, departureDate_out, nome_esteso_partenza, arrivalTime_out, arrivalDate_out, details.luogo_hotel, 
					departureTime_in,departureDate_in,arrivalTime_in,arrivalDate_in,bottone);

			let headHTML = '<title>Riepilogo</title>'+
			'<meta charset="utf-8">'+
			'<meta http-equiv="X-UA-Compatible" content="IE=edge">'+
			'<meta name="description" content="Travelix Project">'+
			'<meta name="viewport" content="width=device-width, initial-scale=1">'+
			'<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">'+
			'<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">'+
			'<link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">'+
			'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">'+
			'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">'+
			'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_styles.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_responsive.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/about_styles.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/about_responsive.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_styles.css">'+
			'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_responsive.css">';

			document.head.innerHTML = headHTML;

			document.getElementById("contenitore").innerHTML = contenuto;
			
			window.history.pushState({}, null, this.url);

			$("#search_form_prenotazione").submit(function(event){

				event.preventDefault();

				effettua_prenotazione(box);				

			});	
		}
	});	
}


function mostra_catalogo(){
	
	let departure = $('#departure').val();
	let destination = $('#destination').val();
	let check_in = $('#check_in').val();
	let check_out = $('#check_out').val();
	let adults = $('#adults').val();
	let children = $('#children').val();

	$.ajax({
		type:'GET',
		url:'viaggio',
		data: {departure: departure, destination: destination,
			check_in: check_in, check_out: check_out,
			adults: adults, children: children},
			dataType: 'json',   
			success:function(json_viaggio){
				
				var size = json_viaggio.length;
				
				if(size==0){
					alert("Nessun risultato trovato!");
					window.location.href = "https://localhost:8443/TravelixCopia/pagine/home.jsp";
				}

				else{
					
					let shortInfo;
					
					let stringa = '';

					for(let i=0; i<size; i++){
						
						shortInfo = json_viaggio[i].infoViaggio.shortInfo;
						
						stringa += viaggi(shortInfo.immagine_hotel, shortInfo.nome_hotel, shortInfo.ID_hotel, shortInfo.ID_volo, 
								shortInfo.stelle_hotel, shortInfo.rating_hotel, shortInfo.prezzo_tot, shortInfo.numeroAdulti, shortInfo.numeroBambini, i);
					}


					let headHTML = '<title>Risultati</title>'+
					'<meta charset="utf-8">'+
					'<meta http-equiv="X-UA-Compatible" content="IE=edge">'+
					'<meta name="description" content="Travelix Project">'+
					'<meta name="viewport" content="width=device-width, initial-scale=1">'+
					'<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">'+
					'<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_styles.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_responsive.css">'+
					'<link rel="stylesheet" type="text/css" href="styles/offers_styles.css">'+
					'<link rel="stylesheet" type="text/css" href="styles/offers_responsive.css">';

					document.head.innerHTML = headHTML;

					document.getElementById("contenitore").innerHTML = stringa;
					
					window.history.pushState({}, null, this.url);

					for(let j=0; j<size; j++){

						$("#search_form_riepilogo"+j).submit(function(e){

							e.preventDefault();

							visualizzaRiepilogo(j);
						});

					}

				}					   
			}
	});

}


