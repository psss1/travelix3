  function statusChangeCallback(response) {
      console.log('statusChangeCallback');
      var token = response.authResponse.accessToken;
      var expiredToken = response.authResponse.expiresIn;
      var stato = response.status;
      console.log('token: ' +token);
      console.log('expiredToken: ' +expiredToken);
      console.log(response);
      if (response.status === 'connected') {
    	  //window.location.reload();
    	  //redirect();
    	    FB.api('/me',{fields: 'name,first_name,last_name'}, function(response) {	
    	        var nome = response.first_name;
    	        var cognome = response.last_name;
     	        window.location.href = "/TravelixCopia/LoginController?name="+ nome+"&surname="+cognome+"&token="+token;
    	      });
      } else if (response.status === 'not_authorized') {
        document.getElementById('status').innerHTML = 'Please log ' +
          'into this app.';
      } else {
        document.getElementById('status').innerHTML = 'Please log ' +
          'into Facebook.';
      }
    }
  


  function checkLoginState() {               // Called when a person is finished with the Login Button.
    FB.getLoginStatus(function() {   // See the onlogin handler
      window.location.reload();
      statusChangeCallback(response);
    });
  }


  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1230843143781356',
      cookie     : true,                     // Enable cookies to allow the server to access the session.
      xfbml      : true,                     // Parse social plugins on this webpage.
      version    : 'v5.0'           // Use this Graph API version for this call.
    });


    FB.getLoginStatus(function(response) {   // Called after the JS SDK has been initialized.
      statusChangeCallback(response);        // Returns the login status.
    });
  };

  
  (function(d, s, id) {                      // Load the SDK asynchronously
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "https://connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
  }(document, 'script', 'facebook-jssdk'));

