function eliminaPrenotazione(idPrenotazione) {
  
  var r = confirm("Sei sicuro di voler eliminare la prenotazione?");
  if (r == true) {
	  
		$.ajax({
			type:'DELETE',
			url: 'prenotazione' + '?' + $.param({"idPrenotazione": idPrenotazione}),
			dataType: 'json',   
			success:function(esitoCancellazione){
				
				let esito = esitoCancellazione.esito.esito;
				
				if(esito){
					alert("Cancellazione avvenuta con successo");
					window.location.href = "https://localhost:8443/TravelixCopia/prenotazione?method=GET";
				}
				
				else
					alert("Errore durante la cancellazione");
			}
		
	});
  }
}

function pagaPrenotazione(idPrenotazione) {
  
  var r = confirm("Procedere al pagamento?");
  if (r == true) {
	  
		$.ajax({
			type:'PUT',
			url: 'prenotazione' + '?' + $.param({"idPrenotazione": idPrenotazione}),
			dataType: 'json',   
			success:function(esitoPagamento){
				
				let esito = esitoPagamento.esito.esito;
				
				if(esito){
					alert("Pagamento avvenuto con successo");
					window.location.href = "https://localhost:8443/TravelixCopia/prenotazione?method=GET";
				}
				
				else
					alert("Errore pagamento");
			}
		
	});
  }
}

function visualizzaDettagli(idPrenotazione) {
	  
			$.ajax({
				type:'GET',
				url:'prenotazione',
				data: {idPrenotazione: idPrenotazione},
				dataType: 'json',   
				success:function(jsonPrenotazione){

					let contenuto;
					
					var box = JSON.stringify(jsonPrenotazione);
					
					let infoPrenotazione = jsonPrenotazione.infoPrenotazione;
					
					let nomeHotel = infoPrenotazione.nomeHotel;
					let stelleHotel = infoPrenotazione.stelleHotel;
					let indirizzoHotel = infoPrenotazione.indirizzoHotel;
					let numeroHotel = infoPrenotazione.numeroHotel;
					let prezzoTotale = infoPrenotazione.prezzoTotale;
					let immagineHotel = infoPrenotazione.immagineHotel;
					let descrizioneHotel = infoPrenotazione.descrizioneHotel;
					
					let numeroAdulti = infoPrenotazione.numeroAdulti;
					let numeroBambini = infoPrenotazione.numeroBambini;
					
					let persone = numeroAdulti+numeroBambini;
					
					let prezzoHotel = infoPrenotazione.prezzoHotel;
					let prezzoVoli = infoPrenotazione.prezzoVoli;
					let luogoDestinazione = infoPrenotazione.luogoDestinazione;
					let luogoOrigine = infoPrenotazione.luogoOrigine;
					
					let orarioVoloAndataOrigine = infoPrenotazione.orarioVoloAndataOrigine;
					let dataVoloAndataOrigine = infoPrenotazione.dataVoloAndataOrigine;
					let orarioVoloAndataDestinazione = infoPrenotazione.orarioVoloAndataDestinazione;
					let dataVoloAndataDestinazione = infoPrenotazione.dataVoloAndataDestinazione;
					let orarioVoloRitornoOrigine = infoPrenotazione.orarioVoloRitornoOrigine;
					let dataVoloRitornoOrigine = infoPrenotazione.dataVoloRitornoOrigine;
					let orarioVoloRitornoDestinazione = infoPrenotazione.orarioVoloRitornoDestinazione;
					let dataVoloRitornoDestinazione = infoPrenotazione.dataVoloRitornoDestinazione;

					if(persone==2){
						camera="Doppia";
						immagine_camera = "images/doppia.jpg";
					}
					else{
						camera="Quadrupla";
						immagine_camera = "images/quadrupla.jpg";
					}
					
					let bottone = false;
					
					contenuto = riepilogoViaggio(nomeHotel, stelleHotel, indirizzoHotel, numeroHotel, prezzoTotale, 
							immagineHotel, descrizioneHotel, persone, prezzoHotel, prezzoVoli, immagine_camera, 
							camera, orarioVoloAndataOrigine, dataVoloAndataOrigine, luogoOrigine, orarioVoloAndataDestinazione, dataVoloAndataDestinazione, luogoDestinazione, 
							orarioVoloRitornoOrigine,dataVoloRitornoOrigine,orarioVoloRitornoDestinazione,dataVoloRitornoDestinazione, bottone);
					
					let headHTML = '<title>Dettagli Prenotazione</title>'+
					'<meta charset="utf-8">'+
					'<meta http-equiv="X-UA-Compatible" content="IE=edge">'+
					'<meta name="description" content="Travelix Project">'+
					'<meta name="viewport" content="width=device-width, initial-scale=1">'+
					'<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">'+
					'<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">'+
					'<link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">'+
					'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">'+
					'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">'+
					'<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_styles.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_responsive.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/about_styles.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/about_responsive.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_styles.css">'+
					'<link rel="stylesheet" type="text/css" href="pagine/styles/offers_responsive.css">';

					document.head.innerHTML = headHTML;

							document.getElementById("contenitore").innerHTML = contenuto;
							
							window.history.pushState({}, null, this.url);

				}
			
		});
	  
	}
	
function stampaBottone(idPrenotazione,statoPagamento,i,tipo){
	
	let contenuto;
	
	var stato = statoPagamento.toString();
	
	if(stato=="true")
		contenuto='<th>Pagamento effettuato</th>';
	
	else{
		
		if(tipo=="paga")
			contenuto='<th><button onclick="pagaPrenotazione('+idPrenotazione+')">Paga</button></th>';
		
		else
			contenuto='<th><button onclick="eliminaPrenotazione('+idPrenotazione+')">Rimuovi</button></th>';
	}
	
	if(tipo=="paga")
		document.getElementById("stampa_bottone_paga"+i).innerHTML = contenuto;
	else
		document.getElementById("stampa_bottone_elimina"+i).innerHTML = contenuto;
		
}

