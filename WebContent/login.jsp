<%-- La servlet che gestisce la login, se tutto va bene mi mette un utente in sessione, cio� crea un nuovo oggetto 
	 che chiama utente e dentro ci mette le informazioni inerenti all'utente. Se l'autenticazione non va a buon fine l'oggetto  
	 utente non deve esser creato ma crea un oggetto errore dove gli mette un campo testuale che � la stringa di errore da visualizzare 
	 e ritorna il controllo alla login
--%>

<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="en">
<head>
<title>Login</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/login.css">
<link rel="stylesheet" type="text/css" href="styles/responsive.css">
</head>

<body style="background-color:#31124b">

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v5.0&appId=1230843143781356&autoLogAppEvents=1"></script>
<script src="scriptLogin/scriptFacebookLogin.js"></script>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">
		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
						<div class="logo_container">
							<div class="logo"><a href="#"><img src="images/logo.png" alt="">travelix</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>

	<div class="menu trans_500">
		<div class="menu_content d-flex flex-column align-items-center justify-content-center text-center">
			<div class="menu_close_container"><div class="menu_close"></div>
			</div>
			<div class="logo menu_logo"><a href="#"><img src="images/logo.png" alt=""></a>
			</div>
			
		</div>
	</div>

	<!-- spazio -->

	<footer class="footer">
		<div class="container">
			<div class="row">
			</div>
		</div>
	</footer>

<%-- Non voglio una semplice richiesta ma una sessione, dalla request risalgo alla sessione. 
	 Ho un oggetto in sessione che ho chiamato error, in sessione posso memorizzare degli oggetti,
	 mi serve quando provo a fare il login e non va a buon fine. 
	 Se errore � diverso da null allora mostrami la stringa dell'errore 
 --%>
<%  HttpSession sessione = request.getSession();
	String error = (String)sessione.getAttribute("error");
    if(error!=null){%>

<%-- "%=error" significa mostrami a video l'errore, la prende perch� l'ha messa la servlet che gestisce la form --%>
<p><%=error %></p><%} %>

	<!-- Login -->


	<div class="login">

		<div class="container">
			<div class="row">
				<!-- login -->
				<div class="col-sm">
					<div class="login_form_container">
						<div class="login_title">Login</div>
						<form id="login_form" class="contact_form" name="f" action="<%=request.getContextPath() %>/LoginController" method="post">	
							<input type="text" id="username" name="username" class="login_form_email input_field" placeholder="Username" required="required" data-error="Email is required.">
							<input type="password" id="password" name="password" class="login_form_password input_field" placeholder="Password" required="required" data-error="Password is required." >
							<h2> <br> </h2>
							<button type="submit" id="form_submit_button" class="form_submit_button button">LOGIN<span></span><span></span><span></span></button>
							
							<h2> <br> </h2>
							<fb:login-button scope="public_profile" type="submit" onlogin="checkLoginState();" data-auto-logout-link="false"></fb:login-button>	
						</form>
						<button class="form_submit_button button">REGISTRAZIONE<span></span><span></span><span></span></button>
					</div>
				</div>
			</div>
		</div>
	</div>


</div>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>

</body>

</html>