<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html lang="en">
<head>
<title>Prenotazioni</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="description" content="Travelix Project">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
<link href="plugins/colorbox/colorbox.css" rel="stylesheet" type="text/css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
<link rel="stylesheet" type="text/css" href="styles/single_listing_styles.css">
<link rel="stylesheet" type="text/css" href="styles/single_listing_responsive.css">
<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_styles.css">
<link rel="stylesheet" type="text/css" href="pagine/styles/single_listing_responsive.css">
</head>

<body>

<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v5.0&appId=1230843143781356&autoLogAppEvents=1"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

<script>
history.pushState({}, null, "https://localhost:8443/TravelixCopia/prenotazione");
</script>



<%@page import="org.json.JSONObject"%>
<%@page import="org.json.JSONArray"%>
<%@page import="org.json.JSONException"%>
<%@page import="org.json.JSONString"%>

<%

String jsonPrenotazioni_str = response.getHeader("jsonPrenotazioni");

JSONArray jsonPrenotazioni = new JSONArray(jsonPrenotazioni_str);
JSONObject index = new JSONObject();
JSONObject box = new JSONObject();

int size = jsonPrenotazioni.length();

int[] idPrenotazione_arr = new int[size];
boolean[] statoPagamento_arr = new boolean[size];
int[] prezzoTotale_arr = new int[size];
String[] dataVoloAndataDestinazione_arr = new String[size];
String[] dataVoloRitornoOrigine_arr = new String[size];
String[] nomeHotel_arr = new String[size];
String[] luogoDestinazione_arr = new String[size];
int[] numeroAdulti_arr = new int[size];
int[] numeroBambini_arr = new int[size];

for(int i=0; i<size; i++){
	
	index = jsonPrenotazioni.getJSONObject(i);
	
	box = index.getJSONObject("infoPrenotazione");
	
	idPrenotazione_arr[i] = box.getInt("idPrenotazione");
	statoPagamento_arr[i] = box.getBoolean("statoPagamento");
	prezzoTotale_arr[i] = box.getInt("prezzoTotale");
	dataVoloAndataDestinazione_arr[i] = box.getString("dataVoloAndataDestinazione");
	dataVoloRitornoOrigine_arr[i] = box.getString("dataVoloRitornoOrigine");
	nomeHotel_arr[i] = box.getString("nomeHotel");
	luogoDestinazione_arr[i] = box.getString("luogoDestinazione");
	numeroAdulti_arr[i] = box.getInt("numeroAdulti");
	numeroBambini_arr[i] = box.getInt("numeroBambini");
	
}

int idPrenotazione;
boolean statoPagamento;
int prezzoTotale;
String dataVoloAndataDestinazione = new String();
String dataVoloRitornoOrigine = new String();
String nomeHotel = new String();
String luogoDestinazione = new String();
int numeroAdulti;
int numeroBambini;
String iconaPagamento = new String();
String statoPagamento_str = new String();
%>

<div class="super_container">
	
	<!-- Header -->

	<header class="header">

		<!-- Top Bar -->

		<div class="top_bar">
			<div class="container">
				<div class="row">
					<div class="col d-flex flex-row">
						<div class="user_box ml-auto">
							<div class="user_box_register user_box_link"><a href="#" onclick="facebookLogout()">logout</a></div>
						</div>
					</div>
				</div>
			</div>		
		</div>

		<!-- Main Navigation -->

		<nav class="main_nav">
			<div class="container">
				<div class="row">
					<div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
						<div class="logo_container">
							<div class="logo"><a href="#"><img src="images/logo.png" alt="">travelix</a>
							</div>
						</div>
						<div class="main_nav_container ml-auto">
							<ul class="main_nav_list">
							<li class="main_nav_item"><a href="<%=request.getContextPath()%>/pagine/home.jsp">home</a></li>		
							<li class="main_nav_item"><a href="https://localhost:8443/TravelixCopia/prenotazione?method=GET">prenotazioni</a></li>		
							</ul>
						</div>
					</div>
				</div>
			</div>
		</nav>
	</header>


	</br>
	</br>
	</br>
	</br>
	</br>

		<!-- Single Listing -->

		<div id="contenitore" class="container">
			<div class="row">
				<div class="col-lg-12">
					<div class="single_listing">
					
							
							<div class="container">
							  <h2>Prenotazioni</h2>
									  
							  <table class="table">
								<thead>
								  <tr>
									<th>ID</th>
									<th>Hotel</th>
									<th>Citt�</th>
									<th>Check-in</th>
									<th>Check-out</th>
									<th>N. Adulti</th>
									<th>N. Bambini</th>									
									<th>Prezzo</th>
									<th>Stato Pagamento</th>
									<th>Cancellazione</th>
									<th>Pagamento</th>
									<th>Dettagli</th>
								  </tr>
								</thead>
								
<%

for(int i=0;i<size;i++){
	
	idPrenotazione = idPrenotazione_arr[i];
	statoPagamento = statoPagamento_arr[i];
	prezzoTotale = prezzoTotale_arr[i];
	dataVoloAndataDestinazione = dataVoloAndataDestinazione_arr[i];
	dataVoloRitornoOrigine = dataVoloRitornoOrigine_arr[i];
	nomeHotel = nomeHotel_arr[i];
	luogoDestinazione = luogoDestinazione_arr[i];
	numeroAdulti = numeroAdulti_arr[i];
	numeroBambini = numeroBambini_arr[i];

	if(statoPagamento)
		iconaPagamento = "images/Pagato.png";
	else
		iconaPagamento = "images/nonPagato.png";
%>			
									<tbody>
								  <tr>
									<th><%=idPrenotazione %></th>
									<th><%=nomeHotel %></th>
									<th><%=luogoDestinazione %></th>
									<th><%=dataVoloAndataDestinazione %></th>
									<th><%=dataVoloRitornoOrigine %></th>
									<th><%=numeroAdulti %></th>
									<th><%=numeroBambini %></th>
									<th><%=prezzoTotale %></th>
									<!--  <th><%=statoPagamento_str %></th>-->
									<th><img src=<%=iconaPagamento %> alt=""></th>
									<th class="dynamic-th-elimina" data-dynamic1='<%=idPrenotazione%>' data-dynamic2='<%=statoPagamento%>' data-dynamic3='<%=i %>' data-dynamic4='elimina' id='stampa_bottone_elimina<%=i%>'></th>
									<th class="dynamic-th-paga" data-dynamic1='<%=idPrenotazione%>' data-dynamic2='<%=statoPagamento%>' data-dynamic3='<%=i %>' data-dynamic4='paga' id='stampa_bottone_paga<%=i%>'></th>
									<th><button onclick="visualizzaDettagli(<%=idPrenotazione%>)" type="button">Visualizza Dettagli</button></th>
								  </tr>
								</tbody>
<%
}
%>
							  </table>

							</div>

						</div>
						</div>
					</div>
				</div>
			</div>
		</div>		
	</div>

</div>

<script type = text/javascript>

$(document).on("ready", function(){
	
	
    $(".dynamic-th-paga").each(function(index, element){
          var $this = $(element);
          stampaBottone($this.attr("data-dynamic1") , $this.attr("data-dynamic2"),$this.attr("data-dynamic3"),$this.attr("data-dynamic4"));            
    });
    
    $(".dynamic-th-elimina").each(function(index, element){
        var $this = $(element);
        stampaBottone($this.attr("data-dynamic1") , $this.attr("data-dynamic2"),$this.attr("data-dynamic3"),$this.attr("data-dynamic4"));            
  });
});
</script>

<script src="scriptHome/scriptFacebook.js"></script>
<script src="scriptPrenotazione/scriptPrenotazione.js"></script>
<script src="scriptHome/stampaDiv.js"></script>
<script src="scriptHome/scriptHome.js"></script>
<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="plugins/parallax-js-master/parallax.min.js"></script>
<script src="plugins/colorbox/jquery.colorbox-min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
<script src="js/single_listing_custom.js"></script>

</body>

</html>