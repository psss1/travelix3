<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html lang="it">
<head>
	<title>Home</title>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="description" content="Travelix Project">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" type="text/css" href="styles/bootstrap4/bootstrap.min.css">
	<link href="plugins/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.carousel.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/owl.theme.default.css">
	<link rel="stylesheet" type="text/css" href="plugins/OwlCarousel2-2.2.1/animate.css">
	<link rel="stylesheet" type="text/css" href="styles/main_styles.css">
	<link rel="stylesheet" type="text/css" href="styles/responsive.css">
</head>

<body>
<script async defer crossorigin="anonymous" src="https://connect.facebook.net/it_IT/sdk.js#xfbml=1&version=v5.0&appId=1230843143781356&autoLogAppEvents=1"></script>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
<script src="scriptHome/scriptFacebook.js"></script>

<script>
history.pushState({}, null, "https://localhost:8443/TravelixCopia/home");
</script>

<% HttpSession sessione = request.getSession();
   String nome= (String)sessione.getAttribute("nome");
   String ruolo= (String)sessione.getAttribute("ruolo");
   String username = (String)sessione.getAttribute("username");
   String esitoCreazione = request.getHeader("esitoCreazione");
   
%>

<div class="super_container">

		<!-- Header -->
		
		<header class="header">
			
			<!-- Top Bar -->
			
			<div class="top_bar">
				<div class="container">
					<div class="row">
						<div class="col d-flex flex-row">
							<div class="user_box ml-auto">
								<div class="user_box_register user_box_link"><a href=# onclick="facebookLogout()">logout</a></div>
							</div>
						</div>
					</div>
				</div>		
			</div>

			<!-- Main Navigation -->
			
			<nav class="main_nav">
				<div class="container">
					<div class="row">
						<div class="col main_nav_col d-flex flex-row align-items-center justify-content-start">
							<div class="logo_container">
								<div class="logo"><a href="#"><img src="images/logo.png" alt="">travelix</a></div>
							</div>
							<div class="main_nav_container ml-auto">
								<ul class="main_nav_list">
									<li class="main_nav_item"><a href="<%=request.getContextPath()%>/pagine/home.jsp">home</a></li>	
									<li class="main_nav_item"><a href="https://localhost:8443/TravelixCopia/prenotazione?method=GET">prenotazioni</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>	
			</nav>
			
	</header>
	
	<div id="contenitore" class="super_container">

		<!-- Home -->
		
		<div class="home">
			
			<!-- Home Slider -->
			
			<div class="home_slider_container">
				
				<div class="owl-carousel owl-theme home_slider">
					
					<!-- Slider Item -->
					<div class="owl-item home_slider_item">
						<div class="home_slider_background" style="background-image:url(images/home_slider.jpg)"></div>
						<div class="home_slider_content text-center">
							<div class="home_slider_content_inner" data-animation-in="flipInX" data-animation-out="animate-out fadeOut">
								<h1>Benvenuto </h1>
								<h1><%=nome %></h1>
							</div>
						</div>
					</div>
				</div>			
			</div>
		</div>
		
		<!-- Search -->
		
		<div class="search">
			
			
			<!-- Search Contents -->
			
			<div class="container fill_height">
				<div class="row fill_height">
					<div class="col fill_height">

						<!-- Search Tabs -->
						
						<div class="search_tabs_container">
							<div class="search_tabs d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start">
								<div class="search_tab active d-flex flex-row align-items-center justify-content-lg-center justify-content-start"><img src="images/suitcase.png" alt=""><span>Hotel + Flight </span><img src="images/departure.png" alt=""></div>
							</div>		
						</div>

						<!-- Search Panel -->
						
					<div class="search_panel active">
					<form id="search_form_ricerca" name="search_form_ricerca" class="search_panel_content d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-lg-between justify-content-start" method="get">
							<div class="search_item">
								<div>departure</div>
								<select id="departure" name="departure" class="dropdown_item_select search_input" required="required">
								<option>Napoli-NAP</option>
								<option>Parigi-ORY</option>
								<option>Rotterdam-RTM</option>
								</select>
							</div>							
							<div class="search_item">
								<div>destination</div>
								<select id="destination" name="destination" class="dropdown_item_select search_input" required="required">
								<option>Amsterdam-AMS</option>
								<option>Agadir-AGA</option>
								<option>Barcellona-BCN</option>
								<option>Malaga-AGP</option>
								</select>							
							</div>
							<div class="search_item">
								<div>check in</div>
								<input id="check_in" name="check_in" type="date" class="check_in search_input" min=' 'placeholder="YYYY-MM-DD" required="required">
							</div>
							<div class="search_item">
								<div>check out</div>
								<input id="check_out" name="check_out" type="date" class="check_out search_input" min=' ' placeholder="YYYY-MM-DD" required="required">
							</div>
										
							<script>
								var today = new Date();
								var dd = today.getDate();
								var mm = today.getMonth()+1; //January is 0!
								var yyyy = today.getFullYear();
								 if(dd<10){
								        dd='0'+dd
								    } 
								    if(mm<10){
								        mm='0'+mm
								    } 
								today = yyyy+'-'+mm+'-'+dd;
								
								document.getElementById("check_in").setAttribute("min", today);
								document.getElementById("check_out").setAttribute("min", today);
							</script>							
							
							<div class="search_item">
								<div>adults</div>
								<select id="adults" name="adults"  class="dropdown_item_select search_input">
									<option>01</option>
									<option>02</option>
									<option>03</option>
									<option>04</option>
									<option>05</option>
									<option>07</option>
									<option>08</option>
									<option>09</option>
									<option>10</option>
									
								</select>
							</div>
							<div class="search_item">
								<div>children</div>
								<select name="children" id="children" class="dropdown_item_select search_input">
									<option>00</option>
									<option>01</option>
									<option>02</option>
									<option>03</option>
									<option>04</option>
									<option>05</option>
								</select>
							</div>
							<button id="bottone_ricerca" class="button search_button">search<span></span><span></span><span></span></button>
						</form>
						</div>
					</div>
				</div>
			</div>		
		</div>
	<!-- Intro -->
	
	<div class="intro">
		<div class="container">
			<div class="row">
				<div class="col">
					<h2 class="intro_title text-center">We have the best tours</h2>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-10 offset-lg-1">
					<div class="intro_text text-center">
					</div>
				</div>
			</div>
			<div class="row intro_items">

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@dnevozhai -->
						<div class="intro_item_background" style="background-image:url(images/intro_1.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							<div class="button intro_button"><div class="button_bcg"></div><a href="#">see more<span></span><span></span><span></span></a></div>
							<div class="intro_center text-center">
								<h1>Mauritius</h1>
							</div>
						</div>
					</div>
				</div>

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@hellolightbulb -->
						<div class="intro_item_background" style="background-image:url(images/intro_2.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							<div class="button intro_button"><div class="button_bcg"></div><a href="#">see more<span></span><span></span><span></span></a></div>
							<div class="intro_center text-center">
								<h1>Greece</h1>
							</div>
						</div>
					</div>
				</div>

				<!-- Intro Item -->

				<div class="col-lg-4 intro_col">
					<div class="intro_item">
						<div class="intro_item_overlay"></div>
						<!-- Image by https://unsplash.com/@willianjusten -->
						<div class="intro_item_background" style="background-image:url(images/intro_3.jpg)"></div>
						<div class="intro_item_content d-flex flex-column align-items-center justify-content-center">
							<div class="button intro_button"><div class="button_bcg"></div><a href="#">see more<span></span><span></span><span></span></a></div>
							<div class="intro_center text-center">
								<h1>Scotland</h1>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>

</div>
</div>



<script type = text/javascript>

$(document).ready(function(){
	
	$("#search_form_ricerca").submit(function(event){
		event.preventDefault();
		
		mostra_catalogo();
		
});
});
</script>

<script src="scriptHome/stampaDiv.js"></script>
<script src="scriptHome/scriptHome.js"></script>

<script src="js/jquery-3.2.1.min.js"></script>
<script src="styles/bootstrap4/popper.js"></script>
<script src="styles/bootstrap4/bootstrap.min.js"></script>
<script src="plugins/OwlCarousel2-2.2.1/owl.carousel.js"></script>
<script src="plugins/easing/easing.js"></script>
<script src="js/custom.js"></script>




</body>